import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ManualOrderService {
  public userSessionSubject = new BehaviorSubject(null)
  
  apiURL = "http://103.41.205.129/api/dashboard"
  account_id :any
  customer : any
  constructor(
    private http: HttpClient
  ) { }





  //USER START
async getUserAccount() {

  let res = await fetch(this.apiURL + '/user/account', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getUserRole() {

  let res = await fetch(this.apiURL + '/user/role', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getUserGroup() {

  let res = await fetch(this.apiURL + '/user/group', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getUserGroupById(group_id) {

  let res = await fetch(this.apiURL + '/user/group/'+group_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getUserMenuAccess() {

  let res = await fetch(this.apiURL + '/user/menu_access', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getUserAccountNotification(account_id,type) {

  let res = await fetch(this.apiURL + '/user/account/'+account_id+'/notification/'+type, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}




async getUserAccountActivity(account_id) {

  let res = await fetch(this.apiURL + '/user/account/' + account_id + '/activity', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getUserAccountTask(account_id,type) {

  let res = await fetch(this.apiURL + '/user/account/' + account_id + '/type/' + type + '/task' , {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async getUserAccountTaskDiscussion(account_id,task_id) {

  let res = await fetch(this.apiURL + '/user/account/'+account_id+'/task_discussion/'+task_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async addUserAccountTaskDiscussion(formvalue){
  
  let res = await fetch(this.apiURL+'/user/task_discussion/add',{
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}


async addUserAccount(formvalue){
  
  let res = await fetch(this.apiURL+'/user/account/add',{
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}


async addUserAccountTask(formvalue){
  
  let res = await fetch(this.apiURL+'/user/task/add',{
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}


async updateUserAccount(user_id,formvalue){
  
  let res = await fetch(this.apiURL+'/user/account/'+user_id+'/edit',{
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}




async addContact(formvalue) {
  let res = await fetch(this.apiURL + '/user/contact/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async deleteContact(contact_id) {
  let res = await fetch(this.apiURL + '/user/contact/'+contact_id+'/delete', {
    method: "DELETE",
    headers: { 'Content-Type': 'application/json' },
   
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async addScope(formvalue) {
  let res = await fetch(this.apiURL + '/client/scope/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async getScope() {

  let res = await fetch(this.apiURL + '/client/scope', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async addAquisition(formvalue) {
  let res = await fetch(this.apiURL + '/client/aquisition/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async updateAquisition(aquisition_id,formvalue){
  
  let res = await fetch(this.apiURL+'/client/aquisition/'+aquisition_id+'/edit',{
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}



async updateScope(id,formvalue){
  
  let res = await fetch(this.apiURL+'/client/scope/'+id+'/edit',{
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}




async getAquisition() {

  let res = await fetch(this.apiURL + '/client/aquisition', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getAquisitionById(aquisition_id) {

  let res = await fetch(this.apiURL + '/client/aquisition/'+aquisition_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getContact() {

  let res = await fetch(this.apiURL + '/user/contact', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async updateContact(contact_id,formvalue){
  
  let res = await fetch(this.apiURL+'/user/contact/'+contact_id+'/edit',{
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }
}






//USER END


//CREATOR START
async getCreator() {

  let res = await fetch(this.apiURL + '/creator/creator', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getCreatorById(creator_id) {

  let res = await fetch(this.apiURL + '/creator/creator/'+creator_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async updateCreator(formvalue,creator_id) {

  

  let res = await fetch(this.apiURL + '/creator/creator/'+creator_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async updateCreatorRate(formvalue,rate_id) {

  

  let res = await fetch(this.apiURL + '/creator/rate_cart/'+rate_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async deleteCreatorRate(rate_id) {

  

  let res = await fetch(this.apiURL + '/creator/rate_cart/'+rate_id+'/delete', {
    method: "DELETE",
    headers: { 'Content-Type': 'application/json' },
  
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}

async deleteInvitedCreator(campaign_id) {

  

  let res = await fetch(this.apiURL + '/creator/creator_campaign/'+campaign_id+'/delete', {
    method: "DELETE",
    headers: { 'Content-Type': 'application/json' },
  
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async addCreator(formvalue) {

  

  let res = await fetch(this.apiURL + '/creator/creator/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async addCreatorRate(formvalue,creator_id) {

  

  let res = await fetch(this.apiURL + '/creator/creator/'+creator_id+'/rate_cart/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async addPerformance(formvalue) {

  

  let res = await fetch(this.apiURL + '/creator/performance/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async updatePerformance(formvalue,performance_id) {

  

  let res = await fetch(this.apiURL + '/creator/performance/'+performance_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async getCreatorContract() {

  let res = await fetch(this.apiURL + '/creator/contract', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}





async getCreatorPersonById(person_id) {

  let res = await fetch(this.apiURL + '/creator/person/' + person_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async getCreatorPersonContact(person_id) {

  let res = await fetch(this.apiURL + '/creator/person/' + person_id + '/contact', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async getCreatorPersonNote(person_id) {

  let res = await fetch(this.apiURL + '/creator/person/' + person_id + '/note', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getCreatorPersonContract(person_id) {

  let res = await fetch(this.apiURL + '/creator/person/' + person_id + '/contract', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getCreatorPersonSocialMetric(person_id) {

  let res = await fetch(this.apiURL + '/creator/person/' + person_id + '/social_metric', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}






async getCreatorPersonRateCart(person_id) {

  let res = await fetch(this.apiURL + '/creator/person/' + person_id + '/rate_cart', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addCampaign(formvalue) {

  

  let res = await fetch(this.apiURL + '/creator/campaign/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async getCampaignById(campaign_id) {

  let res = await fetch(this.apiURL + '/creator/campaign/' + campaign_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getCampaign() {

  let res = await fetch(this.apiURL + '/creator/campaign/', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async getInvitedCreator(campaign_id) {

  let res = await fetch(this.apiURL + '/creator/creator_campaign/campaign/' + campaign_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async updateCampaign(formvalue,campaign_id) {
  let res = await fetch(this.apiURL + '/creator/campaign/'+campaign_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}



async addInvitedCreator(formvalue) {
  let res = await fetch(this.apiURL + '/creator/creator_campaign/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}

async addContract(formvalue,creator_campaign_id) {
  let res = await fetch(this.apiURL + '/creator/creator_campaign/'+creator_campaign_id+'/contract/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async getCreatorCampaignPerson(campaign_id) {

  let res = await fetch(this.apiURL + '/creator/campaign/' + campaign_id + '/person', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getCreatorCampaignNote(campaign_id) {

  let res = await fetch(this.apiURL + '/creator/campaign/' + campaign_id + '/note', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async getContract() {

  let res = await fetch(this.apiURL + '/creator/contract/', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async updateContract(formvalue,contract_id) {
  let res = await fetch(this.apiURL + '/creator/contract/'+contract_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}


//CREATOR END

//CLIENT START

async getClient(client_type) {

  let res = await fetch(this.apiURL + '/client/client?client_type='+client_type, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addClient(formvalue) {

  

  let res = await fetch(this.apiURL + '/client/client/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async updateClient(formvalue,client_id) {
  let res = await fetch(this.apiURL + '/client/client/'+client_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}





  //CLIENT END


  //channel

async getVendor(type) {

  let res = await fetch(this.apiURL + '/channel/vendor?type='+type, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}



async addVendor(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/vendor/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async updateVendor(formvalue,vendor_id) {
  let res = await fetch(this.apiURL + '/channel/vendor/'+vendor_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}



async getVendorProduct() {

  let res = await fetch(this.apiURL + '/channel/product/', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getVendorProductCategory(type) {

  let res = await fetch(this.apiURL + '/channel/product/category?type='+type, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addVendorProductCategory(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/product/category/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}

async updateVendorProductCategory(formvalue,cat_id) {
  let res = await fetch(this.apiURL + '/channel/product/category/'+cat_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}


async deleteVendorProductCategory(cat_id) {
  let res = await fetch(this.apiURL + '/channel/product/category/'+cat_id+'/delete', {
    method: "DELETE",
    headers: { 'Content-Type': 'application/json' },
  
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}





async addVendorProduct(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/product/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async updateVendorProduct(formvalue,product_id) {
  let res = await fetch(this.apiURL + '/channel/product/'+product_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}




async getInventory(type) {

  let res = await fetch(this.apiURL + '/channel/inventory?type='+type, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addInventory(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/inventory/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async updateInventory(formvalue,inventory_id) {
  let res = await fetch(this.apiURL + '/channel/inventory/'+inventory_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}


async addInventoryStock(formvalue,id) {

  

  let res = await fetch(this.apiURL + '/channel/inventory/'+id+'/stock/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async updateInventoryStock(formvalue,inventory_id,stock_id) {
  let res = await fetch(this.apiURL + '/channel/inventory/'+inventory_id+'/stock/'+stock_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}






async getEmployee() {

  let res = await fetch(this.apiURL + '/channel/employee', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getEmployeeById(employee_id) {

  let res = await fetch(this.apiURL + '/channel/employee/'+employee_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addEmployee(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/employee/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async addEmployeeEducation(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/employee/education/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async deleteEmployeeEducation(education_id) {
  let res = await fetch(this.apiURL + '/channel/employee/education/'+education_id+'/delete', {
    method: "DELETE",
    headers: { 'Content-Type': 'application/json' },
   
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}



async addEmployeeExperience(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/employee/experience/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async addEmployeePerformance(formvalue) {

  

  let res = await fetch(this.apiURL + '/channel/employee/performance/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}




async updateEmployeePerformance(formvalue,performance_id) {
  let res = await fetch(this.apiURL + '/channel/employee/performance/'+performance_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}





async deleteEmployeeExperience(experience_id) {
  let res = await fetch(this.apiURL + '/channel/employee/experience/'+experience_id+'/delete', {
    method: "DELETE",
    headers: { 'Content-Type': 'application/json' },
    
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}



async updateEmployee(formvalue,employee_id) {
  let res = await fetch(this.apiURL + '/channel/employee/'+employee_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}





async getCommunity() {

  let res = await fetch(this.apiURL + '/community/community', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getCommunityMember() {

  let res = await fetch(this.apiURL + '/community/member', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async getCommunityMemberById(member_id) {

  let res = await fetch(this.apiURL + '/community/member/'+member_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addCommunityCampaignMember(campaign_id,formvalue) {

  

  let res = await fetch(this.apiURL + '/community/campaign/'+campaign_id+'/member/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async getCommunityTicket() {

  let res = await fetch(this.apiURL + '/community/ticket', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getCommunityTicketById(ticket_id) {

  let res = await fetch(this.apiURL + '/community/ticket/'+ticket_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addTicketDiscussion(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/ticket/discussion/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async updateTicket(ticket_id,formvalue) {

  let res = await fetch(this.apiURL + '/community/ticket/'+ticket_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async updateCommunityCampaignMember(member_id,formvalue) {

  let res = await fetch(this.apiURL + '/community/campaign-member/'+member_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


async addCommunityPointsCollection(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/points/collection/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}

async addMemberPoints(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/points/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async addCustomMemberReward(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/custom_reward/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async addMemberRedeem(reward_id,member_id,formvalue) {

  

  let res = await fetch(this.apiURL + '/community/reward/'+reward_id+'/member/'+member_id+'/redeem/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}

async addMemberReward(formvalue,member_id) {
  

  let res = await fetch(this.apiURL + '/community/member/'+member_id+'/reward/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async addCommunityMember(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/member/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}



async updateCommunityMember(formvalue,member_id) {
  let res = await fetch(this.apiURL + '/community/member/'+member_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}


async updateCommunityCampaign(formvalue,campaign_id) {
  let res = await fetch(this.apiURL + '/community/campaign/'+campaign_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}

async getCommunityPoints() {

  let res = await fetch(this.apiURL + '/community/points', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async updateCommunityPointsCollection(formvalue,collection_id) {
  let res = await fetch(this.apiURL + '/community/points/collection/'+collection_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}



async getPointsLeaderboard() {

  let res = await fetch(this.apiURL + '/community/points/leaderboard', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getCommunityPointsCollection() {

  let res = await fetch(this.apiURL + '/community/points/collection', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getReward() {

  let res = await fetch(this.apiURL + '/community/reward', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addReward(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/reward/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}

async updateRewardRedeem(formvalue,redeem_id) {
  let res = await fetch(this.apiURL + '/community/reward/redeem/'+redeem_id+'/edit', {
    method: "PATCH",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }

}



async getRewardRedeem() {

  let res = await fetch(this.apiURL + '/community/reward/redeem', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}





async getCommunityCampaign() {

  let res = await fetch(this.apiURL + '/community/campaign', {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}

async getCommunityCampaignById(campaign_id) {

  let res = await fetch(this.apiURL + '/community/campaign/'+campaign_id, {
    method: "GET"
  })
  let data = await res.json()
  if (data.status_code != 200) throw data.message_code
  else {
    return data
  }

}


async addCommunityCampaign(formvalue) {

  

  let res = await fetch(this.apiURL + '/community/campaign/add', {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formvalue)
  })
  let data = await res.json()
  if(data.status_code != 200) throw data.message_code
  else{
    return data
  }


}


  //USER MANAGEMENT

  async getMenuList(){
    
    let res = await fetch(this.apiURL+'getMenuList/',{
      method: "GET"
    })
    let data = await res.json()
  
    return data
  }

  async getUserRoleList(){
    
    let res = await fetch(this.apiURL+'getUserRoleList/',{
      method: "GET"
    })
    let data = await res.json()
  
    return data
  }




  async AccountLogin(formvalue){
   
    let res = await fetch(this.apiURL+'/user/login',{
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_code
    else{
      this.userSessionSubject.next(data.data)
      this.setUserSession(data.data.session_id)
      this.account_id = data.data.id
      return data
    }
  }

  async getUserSession(session_id){
   
    let res = await fetch(this.apiURL + '/user/session/' + session_id, {
      method: "GET"
    })
    let data = await res.json()
    if (data.status_code != 200) {
      return false
    }
      
    else {
      this.userSessionSubject.next(data.data)
      this.account_id = data.data.id
      return data
    }
  }


  userLogout(){
    this.userSessionSubject.next(null)
    this.setUserSession(null)
  }

  setUserSession(data){
   
    localStorage.setItem('user_session', data );
   
  }
  getSessionId(){
    var user_session = localStorage.getItem("user_session");
    return user_session
  }


  // getCustomerId(){
  //   var data = JSON.parse(localStorage.getItem('customer_session'));
   
  //   if(data){
  //     this.customer_id = data.entity_id
  //     return this.customer_id
  //   }
  //   else{
  //     return null
  //   }
   
  // }


  
  async getUserAccountList(){
    
    let res = await fetch(this.apiURL+'getUserAccountList/',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  //CREATOR
  async getTaskListByCampaignId(id){
    
    let res = await fetch(this.apiURL+'getTaskListByCampaignId/',{
      method: "GET"
    })
    let data = await res.json()
  
    return data
  }
  
  async getCommentListByCampaignId(id){
    
    let res = await fetch(this.apiURL+'getCommentListByCampaignId/',{
      method: "GET"
    })
    let data = await res.json()
  
    return data
  }

  async addTaskByCampaignId(formvalue){
    
    let res = await fetch(this.apiURL+'addTaskByCampaignId',{
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_code
    else{
      return data
    }
  }






}
