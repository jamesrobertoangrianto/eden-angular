import { TestBed } from '@angular/core/testing';

import { ManualOrderService } from './manual-order.service';

describe('ManualOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManualOrderService = TestBed.get(ManualOrderService);
    expect(service).toBeTruthy();
  });
});
