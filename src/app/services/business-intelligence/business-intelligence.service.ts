import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class BusinessIntelligenceService {
  apiURL = "https://wia.id/wia-module/Api/"


  public loadingToastSubject = new BehaviorSubject(null)

  dialogEvent: EventEmitter<any> = new EventEmitter()
  toastEvent: EventEmitter<any> = new EventEmitter()
  navigation_menu: any

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { 
    this.navigation_menu = [
     
      {
        "id": "1",
        "menu_id": "1",
        "title": "Dashboard",
        "navigation": "/",
        "icon": "task",
        "global_nav": true,
        "has_child": false,
      },
      {
        "id": "2",
        "menu_id": "2",
        "title": "User Management",
        "navigation": "/user/user-account",
        "icon": "faUsers",
        "global_nav": true,
        "has_child": false,
        
      },
      // {
      //   "id": "1",
      //   "menu_id": "1",
      //   "title": "Contact",
      //   "navigation": "/user/contact/",
      //   "icon": "faAddressBook",
      //   "global_nav": true,
      //   "has_child": false,
        
      // },
      {
        "id": "5",
        "menu_id": "5",
        "title": "Creator",
        "navigation": "/creator/",
        "icon": "faUserCircle",
        "has_child": true,
        "child": [
          {
            "id": "8",
            "menu_id": "8",
            "title": "Creator",
            "navigation": "/creator/creator",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "6",
            "menu_id": "6",
            "title": "Campaign",
            "navigation": "/creator/campaign",
            "icon": "title",
            "has_child": false
          },
        
          
          {
            "id": "9",
            "menu_id": "9",
            "title": "Contract",
            "navigation": "/creator/contract",
            "icon": "title",
            "has_child": false
          }
        ]
      },
     
      {
        "id": "10",
        "menu_id": "10",
        "title": "Client",
        "navigation": "/client/",
        "icon": "faUserTie",
        "has_child": true,
        "child": [
         
          {
            "id": "13",
            "menu_id": "13",
            "title": "Client",
            "navigation": "/client/client",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "11",
            "menu_id": "11",
            "title": "Acquisition",
            "navigation": "/client/aquisition",
            "icon": "title",
            "has_child": false
          },
          // {
          //   "id": "14",
          //   "menu_id": "14",
          //   "title": "Newsfeed",
          //   "navigation": "/client/newsfeed",
          //   "icon": "title",
          //   "has_child": false
          // }
        ]
      },
      {
        "id": "15",
        "menu_id": "15",
        "title": "Channel",
        "navigation": "/channel/",
        "icon": "faNetworkWired",
        "has_child": true,
        "child": [
          {
            "id": "16",
            "menu_id": "16",
            "title": "Vendor",
            "navigation": "/channel/vendor",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "17",
            "menu_id": "17",
            "title": "Inventory",
            "navigation": "/channel/inventory",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "19",
            "menu_id": "19",
            "title": "Employee",
            "navigation": "/channel/employee",
            "icon": "title",
            "has_child": false
          }
        ]
      },
      {
        "id": "20",
        "menu_id": "20",
        "title": "Community",
        "navigation": "/community/",
        "icon": "faBullhorn",
        "has_child": true,
        "child": [
          {
            "id": "21",
            "menu_id": "21",
            "title": "Member",
            "navigation": "/community/member",
            "icon": "title",
            "has_child": false
          },
      
          {
            "id": "22",
            "menu_id": "22",
            "title": "Campaign",
            "navigation": "/community/campaign",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "23",
            "menu_id": "23",
            "title": "Rewards",
            "navigation": "/community/rewards",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "28",
            "menu_id": "28",
            "title": "Points",
            "navigation": "/community/points",
            "icon": "title",
            "has_child": false
          },
          {
            "id": "24",
            "menu_id": "24",
            "title": "Ticket",
            "navigation": "/community/ticket",
            "icon": "title",
            "has_child": false
          }
        ]
      },
      {
        "id": "25",
        "menu_id": "25",
        "title": "Creative",
        "navigation": "/creative/",
        "icon": "faMagic",
        "has_child": true,
        "child": [
        
          {
            "id": "27",
            "menu_id": "27",
            "title": "Vendor",
            "navigation": "/creative/vendor",
            "icon": "title",
            "has_child": false
          }
        ]
      },

      // {
      //   "id": "25",
      //   "menu_id": "25",
      //   "title": "Community Account",
      //   "navigation": "/creative/",
      //   "icon": "faMagic",
      //   "has_child": true,
      //   "child": [
        
      //     {
      //       "id": "27",
      //       "menu_id": "27",
      //       "title": "Community Account",
      //       "navigation": "/client-area/community-account",
      //       "icon": "title",
      //       "has_child": false
      //     },
      //     {
      //       "id": "27",
      //       "menu_id": "27",
      //       "title": "Creator Account",
      //       "navigation": "/client-area/creator-account",
      //       "icon": "title",
      //       "has_child": false
      //     },
      //     {
      //       "id": "27",
      //       "menu_id": "27",
      //       "title": "Channel Account",
      //       "navigation": "/client-area/channel-account",
      //       "icon": "title",
      //       "has_child": false
      //     },

      //     {
      //       "id": "27",
      //       "menu_id": "27",
      //       "title": "Creative Account",
      //       "navigation": "/client-area/creative-account",
      //       "icon": "title",
      //       "has_child": false
      //     },
      //   ]
      // }
    ]
  }

  //Auth

  validateMenuAccess(PAGE_ID){
    var user_session = JSON.parse(localStorage.getItem("user_session"));
    if(user_session){
      return true
    }else{
      return false
    }
    
  }


  async showLoading(isBar?: boolean){
    this.loadingToastSubject.next(true)
  }

  async hideLoading(isBar?: boolean){
    this.loadingToastSubject.next(false)
  }

  

  openDialog(title,message:string, callbackButton?: string, callbackFn?: VoidFunction){
    let data = {
      'title' : title,
     'message' : message,
     'callbackButton' : callbackButton,
     'callbackFn' : callbackFn,
    }
   
    this.dialogEvent.emit(data)
    
  }

  
  openToast(message:string, callbackButton?: string, callbackFn?: VoidFunction){
    let data = {
     'message' : message,
     'callbackButton' : callbackButton,
     'callbackFn' : callbackFn,
    }
   
    this.toastEvent.emit(data)
    
  }













}
