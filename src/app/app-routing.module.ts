import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BIDashboardComponent } from './component/BusinessIntelligence/dashboard/dashboard.component';
import { LoginComponent } from './component/BusinessIntelligence/dashboard/userModule/login/login.component';
import { UserAccountComponent } from './component/BusinessIntelligence/dashboard/userModule/user-account/user-account.component';
import { UserRoleComponent } from './component/BusinessIntelligence/dashboard/userModule/user-role/user-role.component';
import { ProfileComponent } from './component/BusinessIntelligence/dashboard/userModule/profile/profile.component';
import { CampaignComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign.component';
import { ContractComponent } from './component/BusinessIntelligence/dashboard/creatorModule/contract/contract.component';
import { CampaignViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign-view/campaign-view.component';
import { SalesPipelineComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline.component';
import { SalesPipelineViewComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline-view/sales-pipeline-view.component';
import { ClientComponent } from './component/BusinessIntelligence/dashboard/clientModule/client/client.component';
import { NewsfeedBackendComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend.component';
import { ChannelVendorComponent } from './component/BusinessIntelligence/dashboard/channelModule/channel-vendor/channel-vendor.component';
import { InventoryComponent } from './component/BusinessIntelligence/dashboard/channelModule/inventory/inventory.component';
import { EmployeeComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee.component';
import { CommunityMemberComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member.component';
import { CommunityCampaignComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign.component';
import { CommunityRewardsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-rewards/community-rewards.component';
import { CommunityTicketComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket.component';
import { AssetsComponent } from './component/BusinessIntelligence/dashboard/channelModule/assets/assets.component';
import { CommunityTicketViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket-view/community-ticket-view.component';
import { CreativeVendorComponent } from './component/BusinessIntelligence/dashboard/creativeModule/creative-vendor/creative-vendor.component';
import { CommunityPointsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-points/community-points.component';
import { CommunityAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/community-account/community-account.component';
import { CreatorAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creator-account/creator-account.component';
import { CreativeAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creative-account/creative-account.component';
import { ChannelAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/channel-account/channel-account.component';
import { GeneralContactComponent } from './component/BusinessIntelligence/dashboard/userModule/general-contact/general-contact.component';
import { CreatorViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator-view/creator-view.component';
import { CreatorComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator.component';
import { EmployeeViewComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee-view/employee-view.component';
import { CommunityMemberViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member-view/community-member-view.component';
import { CommunityCampaignViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign-view/community-campaign-view.component';


const routes: Routes = [

  {
    path: '',
    component: BIDashboardComponent,
    data:{
      front: true
    }
  },
 
   
    {
      path: 'login',children:[
        {
          path:'',
          component: LoginComponent,
         
        },
      
      ]
      
    },

    {
      path: 'user/contact',children:[
        {
          path:'',
          component: GeneralContactComponent
         
        },
      
      ],
      
    },

    {
      path: 'user/user-account',children:[
        {
          path:'',
          component: UserAccountComponent
         
        },
      
      ],
      
    },
    {
      path: 'user/user-role',children:[
        {
          path:'',
          component: UserRoleComponent,
         
         
        },
      
      ]
      
    },

    {
      path: 'user/profile',children:[
        {
          path:'',
          component: ProfileComponent,
         
        },
      
      ]
      
    },

    {
      path: 'creator/campaign',children:[
        {
          path:'',
          component: CampaignComponent,
        },
        {
          path:'view/:id',
          component: CampaignViewComponent,
         
        }
        
      
      ]
      
    },

    {
      path: 'creator/creator',children:[
        {
          path:'',
          component: CreatorComponent,
        },
        {
          path:'view/:id',
          component: CreatorViewComponent,
         
        }
        
      
      ]
      
    },
   
    {
      path: 'creator/contract',children:[
        {
          path:'',
          component: ContractComponent,
         
        },
     
      
      ]
      
    },
 

    {
      path: 'client/aquisition',children:[
        {
          path:'',
          component: SalesPipelineComponent,
        },
        {
          path:'view/:id',
          component: SalesPipelineViewComponent,
         
        }
        
      
      ]
      
    },

    {
      path: 'client/client',children:[
        {
          path:'',
          component: ClientComponent,
        },
      
      
      ]
      
    },
    {
      path: 'client/newsfeed',children:[
        {
          path:'',
          component: NewsfeedBackendComponent,
        },
      
      
      ]
      
    },

    {
      path: 'channel/vendor',children:[
        {
          path:'',
          component: ChannelVendorComponent,
        },
      
      
      ]
      
    },


    {
      path: 'channel/inventory',children:[
        {
          path:'',
          component: InventoryComponent,
        },
      
      
      ]
      
    },

    {
      path: 'channel/assets',children:[
        {
          path:'',
          component: AssetsComponent,
        },
      
      
      ]
      
    },


    {
      path: 'channel/employee',children:[
        {
          path:'',
          component: EmployeeComponent,
        },
        {
          path:'view/:id',
          component: EmployeeViewComponent,
         
        }
      
      
      ]
      
    },


  


    {
      path: 'community/member',children:[
        {
          path:'',
          component: CommunityMemberComponent,
        },
        {
          path:'view/:id',
          component: CommunityMemberViewComponent,
         
        }
      
      
      ]
      
    },


    {
      path: 'community/campaign',children:[
        {
          path:'',
          component: CommunityCampaignComponent,
        },
        {
          path:'view/:id',
          component: CommunityCampaignViewComponent,
         
        }
      
      
      
      ]
      
    },

    {
      path: 'community/rewards',children:[
        {
          path:'',
          component: CommunityRewardsComponent,
        },
      
      
      ]
      
    },

    {
      path: 'community/points',children:[
        {
          path:'',
          component: CommunityPointsComponent,
        },
      
      
      ]
      
    },

    

    {
      path: 'community/ticket',children:[
        {
          path:'',
          component: CommunityTicketComponent,
        },
        {
          path:'view/:id',
          component: CommunityTicketViewComponent,
         
        }
        
      
      
      ]
      
    },


    {
      path: 'creative/vendor',children:[
        {
          path:'',
          component: CreativeVendorComponent,
        },
      
      
      ]
      
    },


    {
      path: 'client-area/community-account',children:[
        {
          path:'',
          component: CommunityAccountComponent,
        },
        {
          path:'view/:id',
          component: CommunityAccountComponent,
         
        }
      
      
      ]
      
    },

    {
      path: 'client-area/creator-account',children:[
        {
          path:'',
          component: CreatorAccountComponent,
        },
        {
          path:'view/:id',
          component: CreatorAccountComponent,
         
        }
      
      
      ]
      
    },

    {
      path: 'client-area/channel-account',children:[
        {
          path:'',
          component: ChannelAccountComponent,
        },
        {
          path:'view/:id',
          component: ChannelAccountComponent,
         
        }
      
      
      ]
      
    },

    {
      path: 'client-area/creative-account',children:[
        {
          path:'',
          component: CreativeAccountComponent,
        },
        {
          path:'view/:id',
          component: CreativeAccountComponent,
         
        }
      
      
      ]
      
    },

   





    
    
   
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
