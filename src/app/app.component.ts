import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManualOrderService } from './services/manual-order/manual-order.service';
import { DeviceDetectorService } from 'ngx-device-detector';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  deviceInfo: any;
  session_id: any;




  constructor(
 
    private webService : ManualOrderService,
    private router: Router,
    private deviceService: DeviceDetectorService
    
  ) {

  }

  ngOnInit(): void {
    this.validateLogin()
   
    
    
  }


  async validateLogin(){
    this.session_id = this.encript(navigator.userAgent)

    try {
    
      let response =  await this.webService.getUserSession( this.webService.getSessionId());
   
      if(response){
        let session_hash = this.encript(response.data.email.toLowerCase()) + this.session_id
        if(parseFloat(response.data.session_id) !== parseFloat(session_hash)){
          this.router.navigate( ['/login'] );
        }
  
      }else{
        this.router.navigate( ['/login'] );
      }
   
      
       
     
     } catch (e) {
       
     
       
       console.log(e)
     } finally {
     
   
     }
     
   
  }


  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  
}
