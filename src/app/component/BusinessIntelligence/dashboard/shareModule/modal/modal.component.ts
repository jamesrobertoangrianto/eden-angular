import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faComment,faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  showModal: boolean;
  @Input() isHalf?: boolean
  @Input() title: any
  @Output() onClose = new EventEmitter()
  constructor() { }
  faTimesCircle=faTimes
  ngOnInit(): void {
    this.showModal = true
  }

  closeModal(){
    console.log('click log')
    this.showModal = false
    this.onClose.emit(false)
  }
}
