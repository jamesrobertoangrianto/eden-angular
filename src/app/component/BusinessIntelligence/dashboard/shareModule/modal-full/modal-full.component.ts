import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modal-full',
  templateUrl: './modal-full.component.html',
  styleUrls: ['./modal-full.component.scss']
})
export class ModalFullComponent implements OnInit {
  @Input() title: any
  @Output() onClose = new EventEmitter()
  showModal: boolean;
  constructor() { }
  faTimesCircle=faTimes

  ngOnInit(): void {
    this.showModal = true
  }
  closeModal(){
    console.log('click log')
    this.showModal = false
    this.onClose.emit(false)
  }

}
