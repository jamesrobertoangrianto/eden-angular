import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {faList, faCheckCircle, faCircle, faChevronUp, faSquare, faChevronRight,faRedo,faChevronDown,faTimes, faFilter, faFileExport,faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-table-action',
  templateUrl: './table-action.component.html',
  styleUrls: ['./table-action.component.scss']
})
export class TableActionComponent implements OnInit {
  faChevronRight=faChevronRight
  faChevronDown=faChevronDown
  faCircle=faCircle
  faCheckCircle=faCheckCircle
  faChevronUp=faChevronUp
  faFilter=faFilter
  faTimes=faTimes
  faRedo=faRedo
  faSearch=faSearch
  faList=faList
  faSquare=faSquare
  faFileExport=faFileExport
  @Input() sortListItems: any
  menu_id: any;
  searchKey: null;
  megaMenu: boolean;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.megaMenu = false
    console.log('woi')
  //  console.log(this.sortListItems)
  }
  openMenu(id){
    console.log(id)
    this.menu_id = id
    // if(this.menu_id){
    //   this.menu_id = null
    // }
  }
  closeMenu(){
   
   setTimeout(()=>{ // this will make the execution after the above boolean has changed
    this.menu_id = null
    },300); 
  }

  selectChild(item,child_item){
    console.log(item)
    item.selected_child = child_item.name+child_item.id
    console.log(child_item)
  }
  removeSelectedChild(item){
    
    item.selected_child = null
  
  }
  clearSearch(){
    this.searchKey = null
  }

  selectMegaMenuChild(item,child_item){
    console.log(item)
    console.log(child_item)
    child_item.click = !child_item.click
  }

  search(){
    console.log('search')
    this.router.navigate(
      [],
      { queryParams: { search: this.searchKey},
      queryParamsHandling: 'merge' }
    );
  }


  toggleMegamenu(){
    this.megaMenu = !this.megaMenu
  }
}
