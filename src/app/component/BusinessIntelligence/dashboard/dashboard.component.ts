import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faCoins, faUsers, faDownload, faSquare, faClock, faCommentAlt, faEllipsisH, faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class BIDashboardComponent implements OnInit{



  faTimesCircle =faClock
  faClock= faClock
  faEllipsisH = faEllipsisH
  faChevronCircleRight= faChevronRight
  faChevronDown=faChevronDown
  faPaperclip=faPaperclip
  faCommentAlt=faCommentAlt
  faSquareFull = faSquare
  faUserPlus= faUserPlus
  calendar_view: any;
  showModal: any;
  show_button: any;
  selected_task: any;
  tab_view: string;
  show_sidebar: boolean;
  user_account: any;
  group: any;
  activity: any;
  task: any;
  selected_task_discussion: any;
  task_message: any;
  tab_menu_list: { id: string; label: string; class: any; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) {
  }

  async ngOnInit() {


    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
      if (this.tab_view) {
     
       console.log(this.user_account)
       this.getUserAccountTask(this.user_account?.id,this.tab_view)
     
      } 
    })



    
    this.webService.userSessionSubject.subscribe(
      (res)=>{
       
      //  console.log(res)
        if(res){
          this.user_account = res

          this.getUserGroupById(this.user_account.group_id)
          this.getUserAccountNotification(this.user_account.id)
          this.getUserAccountTask(this.user_account.id,this.tab_view)
          
        }
      }
    )

    

   
    this.setComponentData()



  }


  async getUserAccountNotification(id){
    
    
    try {
     
      let response = await this.webService.getUserAccountNotification(id,'update')
      this.activity = response.data
    
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
   
     }
  }

  async getUserAccountTask(id,type){
    
     console.log(type)
      console.log(id)

    try {
     
      let response = await this.webService.getUserAccountTask(id,type)
     console.log(response)
     this.task = response.data
    
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
   
     }
  }

  async selectTask(item){
    
   this.selected_task=item
 
    try {
     
      let response = await this.webService.getUserAccountTaskDiscussion(item.owner_id,item.id)
     console.log(response)
      this.selected_task_discussion = response.data

    
   
    
     
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
      this.openModal('taskDetailModal')
     }
  }

  async addTaskDiscussion(){
    let form = {
      'message' : this.task_message,
      'sender_id' : this.webService.account_id,
      'task_id' : this.selected_task.id
    }

    console.log(form)
   
      if(this.webService.account_id &&  this.selected_task.id){
        try {
          this.appService.showLoading()
          let response = await this.webService.addUserAccountTaskDiscussion(form)
        
        
       
        
         
         } catch (e) {
           
           this.appService.openToast(e)
       
           
           console.log(e)
         } finally {
          this.appService.hideLoading()
          this.task_message=null
            let response = await this.webService.getUserAccountTaskDiscussion(this.webService.account_id,this.selected_task.id)
            this.selected_task_discussion = response.data
       
         }
      }
  
     
   }
 
 
   



  getPlaceholderName(item) {

    var first_name =item.first_name.charAt(0);
    var last_name =item.last_name.charAt(0);
    return first_name + last_name
  }

  async  getUserGroupById(group_id){
   try {
        let res = await this.webService.getUserGroupById(group_id)
     
      this.group = res.data.accounts

     
     } catch (e) {
       
       this.appService.openToast(e)
   
     } finally {
     
     }
  
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'personal',
        'label' : 'Personal',
      
        'class' : null,
      },
      {
        'id' : 'assigned',
        'label' : 'Assigned',
     
        'class' : null,
      },
   
      

    
    
     
     
    ]

  }



  showButton(a){
    console.log('dad')
    this.show_button = a
  }
  hideButton(){
    this.show_button = null
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  afterSubmitTask(){
    this.closeModal()
    this.getUserAccountTask(this.user_account.id,this.tab_view)
  }
  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }


 
}
