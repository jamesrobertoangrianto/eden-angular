import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedBackendComponent } from './newsfeed-backend.component';

describe('NewsfeedBackendComponent', () => {
  let component: NewsfeedBackendComponent;
  let fixture: ComponentFixture<NewsfeedBackendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsfeedBackendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedBackendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
