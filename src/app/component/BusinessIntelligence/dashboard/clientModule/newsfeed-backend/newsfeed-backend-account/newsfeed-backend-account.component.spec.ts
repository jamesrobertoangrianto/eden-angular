import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedBackendAccountComponent } from './newsfeed-backend-account.component';

describe('NewsfeedBackendAccountComponent', () => {
  let component: NewsfeedBackendAccountComponent;
  let fixture: ComponentFixture<NewsfeedBackendAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsfeedBackendAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedBackendAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
