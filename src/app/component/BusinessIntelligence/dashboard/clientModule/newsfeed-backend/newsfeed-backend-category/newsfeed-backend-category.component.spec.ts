import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsfeedBackendCategoryComponent } from './newsfeed-backend-category.component';

describe('NewsfeedBackendCategoryComponent', () => {
  let component: NewsfeedBackendCategoryComponent;
  let fixture: ComponentFixture<NewsfeedBackendCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsfeedBackendCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsfeedBackendCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
