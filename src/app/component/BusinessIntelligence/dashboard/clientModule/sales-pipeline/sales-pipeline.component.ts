import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpandArrowsAlt, faExpand, faExpandAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-sales-pipeline',
  templateUrl: './sales-pipeline.component.html',
  styleUrls: ['./sales-pipeline.component.scss']
})
export class SalesPipelineComponent implements OnInit {
  faExpandArrowsAlt=faExpandAlt
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];

  clientForm = new FormGroup({
    aquisition_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    client_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    deal_stage: new FormControl('initiate_contact'),
    owner_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    

    


    
  })
  contract_status: { id: string; label: string; value: string; }[];
  client_list: any[];
  aquisition: any;
  overview: { label: string; value: any; }[];



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
     
    })

    this.getAquisition()

    this.contract_status = [
      {
        'id' : 'deal_stage',
        'label' : 'initiate_contact',
        'value' : 'initiate_contact',
      },
      {
        'id' : 'deal_stage',
        'label' : 'introductori_meeting',
        'value' : 'introductori_meeting',
      },
      {
        'id' : 'deal_stage',
        'label' : 'proposal_sent',
        'value' : 'proposal_sent',
      },
      {
        'id' : 'deal_stage',
        'label' : 'contract_sent',
        'value' : 'contract_sent',
      },
      {
        'id' : 'deal_stage',
        'label' : 'won',
        'value' : 'won',
      },

      {
        'id' : 'deal_stage',
        'label' : 'lost',
        'value' : 'lost',
      },
    
    
    
    ]

  }



  selectClient(client){
    console.log(client)
    this.clientForm.get('client_id').setValue(client.value)
  }
  async searchClient(e){
    this.client_list = []
    try {
      let response = await this.webService.getClient('brand')
      response.data.forEach(client => {
        this.client_list.push(
          {
            'name':'parrent_id',
            'label': client.client_name,
            'value':client.id
          }
        )  
      });
    
     
    } catch (error) {
      
    }
  }


  async getAquisition(){
      
      
    try {
      this.appService.showLoading()
      let response = await this.webService.getAquisition()
    this.aquisition = response.data
   console.log(response)
   
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }
  
  async addAquisition(){
    this.clientForm.get('owner_id').setValue(this.webService.account_id)
    console.log(this.clientForm.value)
    try {
      this.appService.showLoading()
        let response = await this.webService.addAquisition(this.clientForm.value)
      
      this._navigateTo(response.data.id)
      } catch (e) {
        
        this.appService.openToast(e)
        
        console.log(e)
      } finally {
        this.appService.hideLoading()
      
        this.closeModal()
    
      }
  }



  async updateAquisition(item,id){
    let form = {}
    form[item.id] = item.value
  
    console.log(form)
    console.log(id)
      try {
        let response = await this.webService.updateAquisition(id,form)
      
       console.log(response)
       
      } catch (error) {
        
      }
    }

    

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }



  _navigateTo(page) {
    this.router.navigate(
      ['/client/aquisition/view/'+page]
    );
  }

  

}
