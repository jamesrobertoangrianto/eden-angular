import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpand, faExpandAlt, faChevronUp, faAddressBook } from '@fortawesome/free-solid-svg-icons';

import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  clientForm = new FormGroup({
    
    client_type: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    client_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    parrent_id: new FormControl(null),


    location: new FormControl(null),

    


    
  })


  contactForm = new FormGroup({
    
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    work: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    mobile: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    position: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    client_id: new FormControl(null),
  
  })

  
  faAddressBook=faAddressBook
  faChevronDown = faChevronDown
  faChevronUp = faChevronUp
  faExpandAlt=faExpandAlt
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  tab_view: string;
  client: any;
  company_list: any[];
  client_list: any[];
  selected_brand: any;
  selected_contact: any;
  onboard_option: { id: string; value: number; label: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  
  ngOnInit(): void {

    this.getClient()
    this.onboard_option = [
      {
        'id' : 'on_board_status',
        'value' : 1,
        'label' : 'onboard',
      },
      {
        'id' : 'on_board_status',
        'value' :  0,
        'label' : 'pending',
      }
    ]
  }

  setValid(){
   if(this.clientForm.get('client_type').value=='brand'){
    this.clientForm.controls["parrent_id"].setValidators([Validators.required])
    this.clientForm.controls["parrent_id"].updateValueAndValidity()
   }else{
    this.clientForm.controls["parrent_id"].setValidators(null)
    this.clientForm.controls["parrent_id"].updateValueAndValidity()
   }
   
  }

  async addContact(){
      this.contactForm.get('client_id').setValue(this.selected_brand.id)
 
      console.log(this.contactForm.value)
    try {
    this.appService.showLoading()
     let response = await this.webService.addContact(this.contactForm.value)
    
     console.log(response)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
      this.getClient()
      this.closeModal()
  
    }
  }


  async updateContact(id,contact){
   
  
    let form = {}
    form[contact.id] = contact.value

    console.log(form)
  try {

   let response = await this.webService.updateContact(id,form)
  console.log(response)

  } catch (e) {
    
    this.appService.openToast(e)
    
    console.log(e)
  } finally {
   

  }
}


    
  async getClient(){
      
    
    try {
      this.appService.showLoading()
      let response = await this.webService.getClient('company')
    
      this.client = response.data
    
      this.client.forEach(item => {
        item.total_onboard = 0
        item.total_aquisition = 0
        
        
          item.child.forEach(child_item => {
            
            item.total_project += child_item.campaign_count
            item.total_onboard += child_item.on_board_status
            
           
            item.total_aquisition+=child_item.total_aquisition
          });
        
       
      }
      
      );
      console.log(this.client)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
      this.appService.hideLoading()
    }
  }

  _getAquisitionBudget(aquisition){
    let total = 0
    aquisition.forEach(item => {
      if(item.deal_stage == 'won'){
        total += parseFloat(item.budget)
      }
        
    });
   
    return total
  }
  async updateClient(item,id){
  let form = {}
  form[item.id] = item.value

  console.log(form)
  console.log(id)
    try {
      let response = await this.webService.updateClient(form,id)
    
     console.log(response)
     
    } catch (error) {
      
    }
  }


  selectClient(client){
    console.log(client)
    this.clientForm.get('parrent_id').setValue(client.value)
  }
  async searchClient(e){
    this.client_list = []
    try {
      let response = await this.webService.getClient('company')
      response.data.forEach(client => {
        this.client_list.push(
          {
            'name':'parrent_id',
            'label': client.client_name,
            'value':client.id
          }
        )  
      });
    
     
    } catch (error) {
      
    }
  }

  viewContact(item){
    this.selected_brand = item

    this.openModal('viewContactModal')
  }
  plusContact(item){
    this.selected_brand = item

    this.openModal('addContactModal')
  }
  async addClient(){
    console.log(this.clientForm.value)

    try {
      this.appService.showLoading()
      let response = await this.webService.addClient(this.clientForm.value)
      console.log(response)
      if(response.data.client_type == 'company'){
        this.client.unshift(response.data)
        this.getClient()
        this.closeModal()
      }
      if(response.data.client_type == 'brand'){
        this.selected_brand = response.data
        this.closeModal()
        console.log('msk')
         setTimeout(() => {
          this.openModal('addContactModal')
        }, 100)
       
      }
  
    } catch (e) {
      this.appService.openToast(e)
    }
    finally{
      this.appService.hideLoading()
      this.clientForm.reset()
      this.closeModal()
    }
  }
  addMore(){}

  toggleChild(item){
    item.isShow = !item.isShow
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }
  

}
