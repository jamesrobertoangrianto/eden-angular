import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralContactComponent } from './general-contact.component';

describe('GeneralContactComponent', () => {
  let component: GeneralContactComponent;
  let fixture: ComponentFixture<GeneralContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
