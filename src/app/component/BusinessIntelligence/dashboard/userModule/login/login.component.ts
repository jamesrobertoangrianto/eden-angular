import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  page_view: string;
  user_account: any;

  
  loginForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email, Validators.nullValidator]
    }),
    password_hash: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    session_id: new FormControl(null,{ 
     
    }),
  })
  
  emailForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email, Validators.nullValidator]
    }),
    
  })

  resetForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email, Validators.nullValidator]
    }),
    
    password: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    confirmation_code: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    })
  })
  sessions: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    
  ) { }

  ngOnInit(): void {
    // this.webService.userSessionSubject.subscribe(
    //   (res)=>{
    //     console.log(res)
    //   }
    // )
    this.sessions = this.encript(navigator.userAgent)
    this.route.queryParamMap.subscribe(queryParams => {

      this.page_view = queryParams.get("page_view")
      
      if(!this.page_view){
        this.page_view = 'login'
      }

      let email = queryParams.get("email")
      if(email){
        this.resetForm.get('email').setValue(email)
      }
      
    })
    
  }

  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  async userLogin(){

    if(this.loginForm.valid){
      let session_hash = this.encript(this.loginForm.get('email').value.toLowerCase())+this.sessions
      this.loginForm.get('password_hash').patchValue(this.encript(this.loginForm.get('password_hash').value))
      this.loginForm.get('session_id').patchValue(session_hash)
    
   
     

      console.log(this.loginForm.value)

      try {
        this.appService.showLoading()
        let response = await this.webService.AccountLogin(this.loginForm.value)
        console.log(response.data)
        this.router.navigate(
          ['/']
        );
        this.loginForm.reset();
      
      
       } catch (e) {
         
         this.appService.openToast(e)
         this.loginForm.reset();
     
         
         console.log(e)
       } finally {
         this.appService.hideLoading()
       }
    }else{
      this.loginForm.reset();
      this.appService.openToast('Please input correct information')
    }
   
        
      
  }




  _navigateToPage(user) {
    if(user.account_type == 'employee'){
      this.router.navigate(
        ['/']
      );
    }else if(user.account_type == 'community_member'){
      this.router.navigate(
        ['client-area/community-member']
      );
    }
    else if(user.account_type == 'creator'){
      this.router.navigate(
        ['client-area/creator']
      );
    }
   
  }

  _navigateTo(page) {
    this.router.navigate(
      [],
      { queryParams: { page_view: page} }
    );
  }

  async openDialog(){
    this.appService.hideLoading()
  //   this.appService.openDialog("Login","Please login to continue","Submit", async ()=>{
  //     // await this.test()
  //  })
  }

  async openToast(){
    this.appService.openToast("Please login to continue","Submit", async ()=>{
      // await this.test()
   })
  }


}