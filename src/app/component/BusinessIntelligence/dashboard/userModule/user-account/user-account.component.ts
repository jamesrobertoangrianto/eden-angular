import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { validPhoneNumber } from 'src/app/forms/validation';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {
  PAGE_ID = 1
  show: boolean;
  dataItems: any;
  isLoading: boolean;
  sortListItems: any;

  registerForm = new FormGroup({
   
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email , Validators.nullValidator]
    }),
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    username: new FormControl(null,{ 
      
    }),
    last_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
   
    role_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl(true),

    group_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

  
    password_hash: new FormControl(null,{ 
    
    }),
   
  
  })
  user_role_list: any;
  active_modal: any;
  menu_list: any;
  selected_user_account: any;
  page_id: string;
  tab_view: string;
  user_account_list: any;
  user_role: any;
  user_group: any;
  menu_access: any;
  selected_menu_ids: any[];
  tab_menu_list: { id: string; label: string; class: any; }[];

  constructor(
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
    private router : Router,
    private route: ActivatedRoute,
  ) { }


  
  ngOnInit(): void {
    this.setComponentData()
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
      if(this.tab_view == 'account'){
        this.getUserAccount()
      }else if(this.tab_view == 'group'){
        this.getUserGroup()
      }
      else if(this.tab_view == 'role'){
        this.getUserRole()
      }
     
    })
    this.menu_access = this.appService.navigation_menu

    this.selected_user_account = null
    this.selected_menu_ids =[]
     
     
    
   
    

  
      
  }

 transform(item) {

      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
  }



    
updateStatus(item){
    item.status = !item.status
    let form ={
      'status' : item.status
    }
    this.updateUserAccount(item.id,form)
  }

  selectItem(id){
   console.log(id)
    if(this.selected_menu_ids.includes(id)){
      this.selected_menu_ids = this.selected_menu_ids.filter(function(item) {
        return item !== id
    })
    }else{
      this.selected_menu_ids.push(id)
    }
  }

  checkMenu(id){
    if(this.selected_menu_ids.includes(id)){
      return true
    }else{
      return false
    }
  }

  setAccountMenuAccess(){
  
    if(this.selected_user_account){
      let form ={
        'menu_access' : JSON.stringify(this.selected_menu_ids)
      }
      
      this.updateUserAccount(this.selected_user_account.id,form)
      
      
      this.selected_user_account = null
      this.selected_menu_ids =[]
     
       
      this.toggleModal('setAccessMenuModal')
    
      this.appService.openToast('Menu access update!')
    }
   
  }
   
  async  updateUserAccount(account_id,form){
   
 
    try {
      
        let res = await this.webService.updateUserAccount(account_id,form)
      
       this.getUserAccount()
    
     } catch (e) {
       
       this.appService.openToast(e)
   
     } finally {
     
       //this.appService.hideLoading()
     }
  
  }



  
  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'account',
        'label' : 'Account',
       
        'class' : null,
      },
      {
        'id' : 'role',
        'label' : 'Role',
      
        'class' : null,
      },
      {
        'id' : 'group',
        'label' : 'Group',
       
        'class' : null,
      },
    
   
     
    ]
  }

  
  async getUserRole(){
    try {
    
    
        let res = await this.webService.getUserRole()
        console.log(res.data)

      this.user_role = res.data
      
    
    
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
     
     
     }
  
  }


  
  async getUserAccount(){
    console.log('get account')
    try {
      this.isLoading = true
      //this.appService.showLoading()
        let res = await this.webService.getUserAccount()
        console.log(res.data)

        this.user_account_list = res.data
       
        console.log(this.sortListItems)
      
    
    
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
      this.isLoading = false
       //this.appService.hideLoading()
     }
  
  }


  async getUserGroup(){
    try {
      this.isLoading = true
      //this.appService.showLoading()
        let res = await this.webService.getUserGroup()
        console.log(res.data)

        this.user_group = res.data
       

      
    
    
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
      this.isLoading = false
      //  this.appService.hideLoading()
     }
  
  }

  


  async addUserAccount(){
   
   this.registerForm.get('username').patchValue(this.registerForm.get('email').value)
   this.registerForm.get('status').patchValue(1)
    this.registerForm.get('password_hash').patchValue(this.encript(this.registerForm.get('password_hash').value))
    
   console.log(this.registerForm.value)
    try {
      this.isLoading = true
      this.appService.showLoading()
        let res = await this.webService.addUserAccount(this.registerForm.value)
        console.log(res.data)
        this.getUserAccount()
        this.appService.openToast(res.message_code)

      
    
    
     } catch (e) {
       
       this.appService.openToast(e)
   
       
       console.log(e)
     } finally {
        this.isLoading = false
        this.toggleModal('registerFormModal')
        this.registerForm.reset()
        this.appService.hideLoading()
     }
  
  }

  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }

  
  setMenuAccess(item){
    console.log(item)
    this.selected_user_account = item
    if(this.selected_user_account.menu_access){
      this.selected_menu_ids = JSON.parse(this.selected_user_account.menu_access)
    }else{
      this.selected_menu_ids=[]
    }
    this.toggleModal('setAccessMenuModal')
  }

  toggleModal(modal_name){
    if(this.active_modal){
      this.active_modal = false
    }
    else{
      this.active_modal = modal_name
    }
    
  }
 
}
