import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreativeAccountComponent } from './creative-account.component';

describe('CreativeAccountComponent', () => {
  let component: CreativeAccountComponent;
  let fixture: ComponentFixture<CreativeAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreativeAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreativeAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
