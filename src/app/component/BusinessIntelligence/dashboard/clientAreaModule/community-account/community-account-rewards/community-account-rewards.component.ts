import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-community-account-rewards',
  templateUrl: './community-account-rewards.component.html',
  styleUrls: ['./community-account-rewards.component.scss']
})
export class CommunityAccountRewardsComponent implements OnInit {

  
  
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
    })
    
    this.setComponentData()
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'collection',
        'label' : 'Collection',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'account_balance',
        'label' : 'Account Balance',
        'count' : 10,
        'class' : null,
      },

    
      {
        'id' : 'report',
        'label' : 'Report',
        'count' : 10,
        'class' : null,
      },
     
     
    ]
    
  }


  _navigateToTabView(tab_view) {
    this.router.navigate(
      [],
      { queryParams: { tab_view: tab_view},
      queryParamsHandling: 'merge' }
    );
  }

  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
   
  }

}
