import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityAccountRewardsComponent } from './community-account-rewards.component';

describe('CommunityAccountRewardsComponent', () => {
  let component: CommunityAccountRewardsComponent;
  let fixture: ComponentFixture<CommunityAccountRewardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityAccountRewardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityAccountRewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
