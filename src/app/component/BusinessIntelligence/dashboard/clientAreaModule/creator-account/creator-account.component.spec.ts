import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorAccountComponent } from './creator-account.component';

describe('CreatorAccountComponent', () => {
  let component: CreatorAccountComponent;
  let fixture: ComponentFixture<CreatorAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
