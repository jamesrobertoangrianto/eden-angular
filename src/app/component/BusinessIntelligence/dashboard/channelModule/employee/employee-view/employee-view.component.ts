import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.scss']
})
export class EmployeeViewComponent implements OnInit {
  faChevronDown = faChevronDown
  faChevronLeft=faChevronLeft
  employee: any;
  employee_id: string;
  overview: {}[];
  showModal: any;


  experienceForm = new FormGroup({
    company_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    industry: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    position: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    experience_time: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    employee_id: new FormControl(null),
    
    
  })


  educationForm = new FormGroup({
    education_level: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    education_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

  
    major: new FormControl(null),
    graduate_year: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    employee_id: new FormControl(null),
    
    
    
  })
  employee_status: { id: string; label: string; value: string; class: string; }[];
  


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.employee_id = params.get("id")
      this.getEmployeeById(this.employee_id)
    })

    this.employee_status = [
      {
        'id' : 'status',
        'label' : 'new_applicant',
        'value' : 'new_applicant',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'interviewed',
        'value' : 'interviewed',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'candidates',
        'value' : 'candidates',
        'class' : 'paid',
      },
    
      {
        'id' : 'status',
        'label' : 'on_board',
        'value' : 'on_board',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'off_board',
        'value' : 'off_board',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'rejected',
        'value' : 'rejected',
        'class' : 'paid',
      },
    ]
    
  }



  async getEmployeeById(id){
    try {
      this.appService.showLoading()
      let res = await this.webService.getEmployeeById(id)
      this.employee = res.data
      this.employee.age = this._getAge(this.employee.dob)
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  _getAge(dob){
    if(dob){
      var today = new Date()
      var receive = new Date(dob)
      let time = Math.round((today.getTime() - receive.getTime())/(1000*60*60*24*365));
     return time + ' Years'
    }
   
  }


  async updateEmployee(item){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    if(this.employee.id){
      try {
     
        let res = await this.webService.updateEmployee(form,this.employee.id)
   
        console.log(res)
      
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       this.appService.openToast('Updated!')
      }
    }
   
  }


  async deleteEmployeeEducation(item){
    try {
     
      let res = await this.webService.deleteEmployeeEducation(item.id)
 
      if(res.data){
       let index= this.employee.education.indexOf(item)
       this.employee.education.splice(index,1)
      }
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
     this.appService.openToast('Updated!')
    }
  }


  async deleteEmployeeExperience(item){
    try {
     
      let res = await this.webService.deleteEmployeeExperience(item.id)
 
      if(res.data){
       let index= this.employee.experience.indexOf(item)
       this.employee.experience.splice(index,1)
      }
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
     this.appService.openToast('Updated!')
    }
  }


  async addEmployeeEducation(){
   this.educationForm.get('employee_id').setValue(this.employee_id)
   console.log(this.educationForm.value)
    try {
      this.appService.showLoading()
      let res = await this.webService.addEmployeeEducation(this.educationForm.value)
      this.employee.education.unshift(res.data)
      console.log(res.data)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
     this.appService.openToast('Updated!')
     this.educationForm.reset()
     this.closeModal()
    }
  }

  async addEmployeeExperience(){
    this.experienceForm.get('employee_id').setValue(this.employee_id)
    console.log(this.experienceForm.value)
     try {
       this.appService.showLoading()
       let res = await this.webService.addEmployeeExperience(this.experienceForm.value)
       this.employee.experience.unshift(res.data)
       console.log(res.data)
     
     } catch (error) {
       this.appService.openToast(error)
     }
     finally{
       this.appService.hideLoading()
      this.appService.openToast('Updated!')
      this.experienceForm.reset()
      this.closeModal()
     }
   }



  generatePerformance(){
    let form = [
      {
        'performance_name' :'Work Ethic',
        'performance_score' : null,
        'employee_id' : this.employee.id,
      },
      {
        'performance_name' :'Communication',
        'performance_score' : null,
        'employee_id' : this.employee.id,
      
      },
      {
        'performance_name' :'Quality of Work',
        'performance_score' : null,
        'employee_id' : this.employee.id,
      },
      {
        'performance_name' :'Teamwork',
        'performance_score' : null,
        'employee_id' : this.employee.id,
      
      },
      {
        'performance_name' :'Time Management',
        'performance_score' : null,
        'employee_id' : this.employee.id,
      
      },

      
    ]
    form.forEach(element => {
      this.addEmployeePerformance(element)
    });  
  }


async addEmployeePerformance(form){
   

  try {
    this.appService.showLoading()
   let response = await this.webService.addEmployeePerformance(form)
  this.employee.performance.unshift(response.data)
 
} catch (e) {
  
  this.appService.openToast(e)
  
  console.log(e)
} finally {
  this.appService.hideLoading()
}

}


async updateEmployeePerformance(item){
  let form = {
     
    'performance_score' : item.value,
    'create_by' : this.webService.account_id
    
  }
  console.log(item)
  try {
    this.appService.showLoading()
   let response = await this.webService.updateEmployeePerformance(form,item.id)
   console.log(response)
 
} catch (e) {
  
  this.appService.openToast(e)
  
  console.log(e)
} finally {
  this.appService.hideLoading()
}

}


  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

}
