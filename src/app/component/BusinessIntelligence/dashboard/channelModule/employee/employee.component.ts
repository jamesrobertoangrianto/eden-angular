import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faClock, faGraduationCap, faBookOpen, faBriefcase } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  faClock= faClock
  faBriefcase=faBriefcase
  faGraduationCap=faGraduationCap
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker

  showModal: any;
  tab_view: string;
  employee: any;



  employeForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    last_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    dob: new FormControl(),

    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    phone: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl('new_applicant'),

  
  
    
    
  })
  employee_status: { id: string; label: string; value: string; class: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }


  
  
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
      this.getEmployee()
    })


    this.employee_status = [
      {
        'id' : 'status',
        'label' : 'new_applicant',
        'value' : 'new_applicant',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'interviewed',
        'value' : 'interviewed',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'candidates',
        'value' : 'candidates',
        'class' : 'paid',
      },
    
      {
        'id' : 'status',
        'label' : 'on_board',
        'value' : 'on_board',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'off_board',
        'value' : 'off_board',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'rejected',
        'value' : 'rejected',
        'class' : 'paid',
      },
    ]


  }



  async getEmployee(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getEmployee()
      this.employee = res.data
      this.employee.forEach(item => {
        item.total_performance = this._getPerformance(item.performance)
        item.age = this._getAge(item.dob)
      });

      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    }
  }


  async updateEmployee(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    if(id){
      try {
     
        let res = await this.webService.updateEmployee(form,id)
   
        console.log(res)
      
      } catch (error) {
        this.appService.openToast(error)
      }
      finally{
       this.appService.openToast('Updated!')
      }
    }
   
  }

  _getAge(dob){
    if(dob){
      var today = new Date()
      var receive = new Date(dob)
      let time = Math.round((today.getTime() - receive.getTime())/(1000*60*60*24*365));
     return 'Age '+time
    }
   
  }

  _getPerformance(performance){
    let total = 0
    performance.forEach(item => {
      total += parseFloat(item.performance_score) / 5
    });

    if( total > 67 && total < 100){
      return 'best'
    }
    else if( total > 37 && total < 67){
      return 'good'
    }
    else if( total >0 && total< 37){
      return 'bad' 
    }
    else{
      return 'not set'
    }

  }

  async addEmployee(con){
    try {
      this.appService.showLoading()
      let res = await this.webService.addEmployee(this.employeForm.value)
  
      if(res.data.id){
        console.log(res.data)
        this.employee.unshift(res.data)
        if(con){
          this._navigateTo(res.data.id)
        }
        this.appService.openToast('New Employee Added!')
     
      }
    
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.employeForm.reset()
      this.appService.hideLoading()
      this.closeModal()
    }
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/channel/employee/view/'+page]
    );
  }

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }


}
