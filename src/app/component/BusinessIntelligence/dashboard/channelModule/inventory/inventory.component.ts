import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  faChevronDown = faChevronDown
  faPenSquare = faPenSquare
  faStickyNote = faStickyNote
  faEnvelope = faEnvelope
  faPhone = faPhone
  faUpload = faUpload
  faChevronRight = faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft = faChevronLeft
  faCalendar = faCalendarAlt
  faMapMarked = faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;

  contract_status: {
    id: string;label: string;class: string;
  } [];
  booking_status: {
    id: string;label: string;class: string;
  } [];
  tab_view: string;
  tab_menu_list: {
    id: string;label: string;class: any;
  } [];
  assets: any;
  product: any;
  selected_inventory: any;



  stockForm = new FormGroup({
    sender: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    stock_in: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    stock_out: new FormControl(0),
    expired_date: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    variant: new FormControl(null),
    location: new FormControl(null),
    inventory_id: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),








  })


  inventoryForm = new FormGroup({
    type: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    name: new FormControl(null, {
      validators: [Validators.required, Validators.nullValidator]
    }),
    brand: new FormControl(null),
    status: new FormControl('in_use'),

    category: new FormControl(null),
    expired_date: new FormControl(null),
    location: new FormControl(null),
    cost: new FormControl(null),

  })
  assets_status: { id: string; label: string; value: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService: BusinessIntelligenceService,
    private webService: ManualOrderService,
  ) {}



  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
      if (this.tab_view == 'assets') {
        this.getAssets()
        console.log('ds')
      } else if (this.tab_view == 'product') {
        this.getProduct()
      }

    })

    this.tab_menu_list = [

      {
        'id': 'product',
        'label': 'Product',
        'class': null,
      },
      {
        'id': 'assets',
        'label': 'Assets',
        'class': null,
      },

    ]


    this.assets_status = [
      {
        'id' : 'status',
        'label' : 'in_use',
        'value' : 'in_use',
      },
      {
        'id' : 'status',
        'label' : 'in_service',
        'value' : 'in_service',
      },
      {
        'id' : 'status',
        'label' : 'in_storage',
        'value' : 'in_storage',
      },
      {
        'id' : 'status',
        'label' : 'missing',
        'value' : 'missing',
      },
      {
        'id' : 'status',
        'label' : 'sold',
        'value' : 'sold',
      },
      {
        'id' : 'status',
        'label' : 'expired',
        'value' : 'expired',
      }
    ]



  }





  async getAssets() {
    try {
      this.appService.showLoading()
      let res = await this.webService.getInventory('assets')
      this.assets = res.data
      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }



  _getAge(start_date) {
    var today = new Date()
    var receive = new Date(start_date)
    let time = Math.round((today.getTime() - receive.getTime()) / (1000 * 60 * 60 * 24));
    return time + ' Day(s) Age'
  }

  _getExpired(end_date) {
    var today = new Date()
    var expired = new Date(end_date)
    let time = Math.round((expired.getTime() - today.getTime()) / (1000 * 60 * 60 * 24));
    if (time < 0) {
      return 'Expired'
    } else {
      return time + ' Day(s) Left'
    }

  }


  async getProduct() {
    try {
      this.appService.showLoading()
      let res = await this.webService.getInventory('product')
      this.product = res.data
      this.calculateTotalStock()
      console.log(res)

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.hideLoading()
    }
  }

  calculateTotalStock() {
    if (this.product) {
      this.product.forEach(item => {
        item.total_stock = this._getAvailableStock(item.stock)
      });
    }

  }
  _getAvailableStock(stock) {
    let total = 0
    stock.forEach(item => {
      total += item.stock_in - item.stock_out
    });
    return total
  }

  async updateStock(action, item) {
    if (action == 'add') {
      item.stock_out = item.stock_out + 1
    } else if (action == 'reduce') {
      item.stock_out = item.stock_out - 1
    }
    console.log(item.stock_out)
    let form = {}
    form['stock_out'] = item.stock_out
    try {


      let res = await this.webService.updateInventoryStock(form, item.inventory_id,item.id)

      console.log(res)
      this.appService.openToast('Stock Update!')
      this.calculateTotalStock()
    } catch (error) {
      this.appService.openToast(error)
    } finally {

    }
  }
  receiveStock(item) {
    this.selected_inventory = item
    this.openModal('addStockModal')
  }
  async updateInventoryStock(event, item) {
    let form = {}
    form[event.id] = event.value
   
    try {

      this.appService.showLoading()
      let res = await this.webService.updateInventoryStock(form, item.inventory_id,item.id)

      console.log(res)
      this.calculateTotalStock()
    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.openToast('Stock Update!')
      this.appService.hideLoading()
    }
  }



  async updateInventory(event, id) {
    let form = {}
    form[event.id] = event.value
    console.log(form)
    try {

      this.appService.showLoading()
      let res = await this.webService.updateInventory(form, id)

      console.log(res)
    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.appService.openToast('Stock Update!')
      this.appService.hideLoading()
    }
  }


  async addInventory() {
    console.log(this.inventoryForm.value)
    try {

      this.appService.showLoading()
      let res = await this.webService.addInventory(this.inventoryForm.value)
      if (res.data) {

        this.appService.openToast('Inventory Added!')
        if (this.inventoryForm.get('type').value == 'product') {
          this.getProduct()
        } else if (this.inventoryForm.get('type').value == 'assets') {
          this.getAssets()
        }
      }



    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.inventoryForm.reset()
      this.appService.hideLoading()
      this.closeModal()


    }
  }



  async addInventoryStock() {
    console.log(this.stockForm.value)
    try {
      this.stockForm.get('inventory_id').setValue(this.selected_inventory.id)
      this.appService.showLoading()

      let res = await this.webService.addInventoryStock(this.stockForm.value, this.selected_inventory.id)
      console.log(res.data)

      res.data.isNew = true
      this.selected_inventory.stock.unshift(res.data)
      this.calculateTotalStock()

    } catch (error) {
      this.appService.openToast(error)
    } finally {
      this.stockForm.reset()
      this.closeModal()
      this.appService.hideLoading()
      this.appService.openToast('Stock Added!')
    }
  }

  toggleShow(item) {
    item.isShow = !item.isShow
  }

  openModal(modal) {
    this.showModal = modal
  }
  closeModal() {
    this.showModal = null
  }



  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/' + page]
    );
  }

}
