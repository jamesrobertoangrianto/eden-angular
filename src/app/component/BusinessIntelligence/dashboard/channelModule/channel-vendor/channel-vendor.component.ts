    import { Component, OnInit } from '@angular/core';
    import { FormGroup, FormControl, Validators } from '@angular/forms';
    import { ActivatedRoute, Router } from '@angular/router';
    import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faAddressBook } from '@fortawesome/free-solid-svg-icons';
    import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
    import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
  @Component({
    selector: 'app-channel-vendor',
    templateUrl: './channel-vendor.component.html',
    styleUrls: ['./channel-vendor.component.scss']
  })
  export class ChannelVendorComponent implements OnInit {
    faAddressBook = faAddressBook
    faChevronDown = faChevronDown

    show_columns: boolean;
    selected_columns: any;
    columns_view: string;
    showModal: any;
    payment_status: {} [];
    contract_status: {
      id: string;label: string;class: string;
    } [];
    booking_status: {
      id: string;label: string;class: string;
    } [];
    tab_view: string;
    tab_menu_list: {
      id: string;label: string;class: any;
    } [];
    vendor: any;




    contactForm = new FormGroup({

      name: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      work: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      mobile: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      position: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      vendor_id: new FormControl(null),

    })








    vendorForm = new FormGroup({
      name: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      type: new FormControl('channel'),



      category_id: new FormControl(null),

      phone: new FormControl(null),
      rating_name: new FormControl('not-set'),
      website: new FormControl(null),

    })

    productForm = new FormGroup({
      name: new FormControl(null, {
        validators: [Validators.required, Validators.nullValidator]
      }),
      type: new FormControl('channel'),
      vendor_id: new FormControl(null),

      category_id: new FormControl(null),

      price: new FormControl(null),


      stock: new FormControl(null),

    })
    product: any;
    selected_vendor: any;
    rating: {
      id: string;label: string;value: string;
    } [];
    category: any;
    category_option: any;

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private appService: BusinessIntelligenceService,
      private webService: ManualOrderService,
    ) {}

    ngOnInit(): void {
      this.route.queryParamMap.subscribe(queryParams => {
        this.tab_view = queryParams.get("tab_view")
        if (this.tab_view == 'vendor') {
          this.getVendor()
        } else if (this.tab_view == 'product') {
          this.getVendorProduct()
        } else if (this.tab_view == 'category') {
          this.getVendorProductCategory()
        }
        this.getVendorProductCategory()


      })



      this.tab_menu_list = [{
          'id': 'vendor',
          'label': 'Vendor',

          'class': null,
        },
        {
          'id': 'product',
          'label': 'Product',

          'class': null,
        },
        {
          'id': 'category',
          'label': 'Category',

          'class': null,
        },







      ]

      this.rating = [

        {
          'id': 'rating_name',
          'label': 'bad',
          'value': 'bad',

        },
        {
          'id': 'rating_name',
          'label': 'good',
          'value': 'good',

        },
        {
          'id': 'rating_name',
          'label': 'best',
          'value': 'best',

        },


      ]

    }

    async searchCategory(e) {
      this.category_option = []
      this.category.forEach(item => {
        this.category_option.push({
          'id': 'category_id',
          'label': item.name,
          'value': item.id
        })
      });


    }
    selectCategory(e) {
      console.log(e)
      this.vendorForm.get('category_id').setValue(e.value)
      this.productForm.get('category_id').setValue(e.value)
    }

    selectTabView($event) {}
    async getVendor() {
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendor('channel')
        this.vendor = res.data
        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    async getVendorProduct() {
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendorProduct()
        this.product = res.data
        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    async getVendorProductCategory() {
      try {
        this.appService.showLoading()
        let res = await this.webService.getVendorProductCategory('channel')
        this.category = res.data
        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }
    async addVendorProductCategory() {
      let form = {
        'type':'creative'
      }
      try {
        this.appService.showLoading()
        let res = await this.webService.addVendorProductCategory(form)
        if (res.data) {
          this.category.unshift(res.data)
          this.appService.openToast('category added!')
        }

        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    async updateVendorProductCategory(e, id) {
      let form = {}
      form[e.id] = e.value
      console.log(form)
      try {
        this.appService.showLoading()
        let res = await this.webService.updateVendorProductCategory(form, id)
        if (res.data) {

          this.appService.openToast('updated!')
        }

        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }

    async deleteVendorProductCategory(item) {

      try {
        this.appService.showLoading()
        let res = await this.webService.deleteVendorProductCategory(item.id)
        if (res.data) {
          let index = this.category.indexOf(item)
          this.category.splice(index, 1)
          this.appService.openToast('updated!')
        }

        console.log(res)

      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.appService.hideLoading()
      }
    }



    toggleShow(item) {
      item.isShow = !item.isShow
    }
    async addVendor() {
      console.log(this.vendorForm.value)

      try {
        this.appService.showLoading()
        let res = await this.webService.addVendor(this.vendorForm.value)
        this.getVendor()

        this.appService.openToast('New vendor added!')
      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.vendorForm.reset()
        this.appService.hideLoading()
        this.closeModal()
      }
    }

    async updateVendor(e, id) {

      let form = {}
      form[e.id] = e.value
      console.log(form)
      try {

        let res = await this.webService.updateVendor(form, id)
        if (res.data) {
          this.appService.openToast('Updated!')
        }

      } catch (error) {
        this.appService.openToast(error)
      } finally {


      }
    }

    createProduct(item) {
      console.log(item)
      this.selected_vendor = item
      this.openModal('addProductModal')
    }


    async updateProduct(e, id) {

      let form = {}
      form[e.id] = e.value
      console.log(form)
      try {

        let res = await this.webService.updateVendorProduct(form, id)

        if (res.data) {
          this.appService.openToast('Updated!')
        }
      } catch (error) {
        this.appService.openToast(error)
      } finally {


      }
    }

    createContact(item) {
      this.selected_vendor = item
      this.openModal('addContactModal')
    }

    async addContact() {
      this.contactForm.get('vendor_id').setValue(this.selected_vendor.id)


      try {
        this.appService.showLoading()
        let response = await this.webService.addContact(this.contactForm.value)
        this.selected_vendor.contact.unshift(response.data)
        console.log(response)
      } catch (e) {

        this.appService.openToast(e)

        console.log(e)
      } finally {
        this.contactForm.reset()
        this.appService.hideLoading()

        this.closeModal()

      }
    }

    async updateContact(id, contact) {


      let form = {}
      form[contact.id] = contact.value

      console.log(form)
      try {

        let response = await this.webService.updateContact(id, form)
        console.log(response)

      } catch (e) {

        this.appService.openToast(e)

        console.log(e)
      } finally {


      }
    }

    async addVendorProduct(item ? ) {
      if (item) {
        this.selected_vendor = item
      }
      console.log(item)
      this.productForm.get('vendor_id').setValue(this.selected_vendor.id)
      console.log(this.productForm.value)
      try {

        let res = await this.webService.addVendorProduct(this.productForm.value)
        if (res.data) {
          res.data.isNew = true
          this.selected_vendor.product_count = this.selected_vendor.product_count + 1
          this.selected_vendor.product.unshift(res.data)
          this.appService.openToast('New Product Added!')
        }



      } catch (error) {
        this.appService.openToast(error)
      } finally {
        this.productForm.reset()
        this.closeModal()
      }


    }


    _navigateToTabView(tab_view) {
      this.router.navigate(
        [], {
          queryParams: {
            tab_view: tab_view
          },
          queryParamsHandling: 'merge'
        }
      );
    }

    openModal(modal) {
      this.showModal = modal
    }
    closeModal() {
      this.showModal = null
    }

  }
