import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityPointsComponent } from './community-points.component';

describe('CommunityPointsComponent', () => {
  let component: CommunityPointsComponent;
  let fixture: ComponentFixture<CommunityPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
