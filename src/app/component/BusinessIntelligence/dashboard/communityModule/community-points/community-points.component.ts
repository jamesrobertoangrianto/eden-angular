import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';


@Component({
  selector: 'app-community-points',
  templateUrl: './community-points.component.html',
  styleUrls: ['./community-points.component.scss']
})
export class CommunityPointsComponent implements OnInit {

  
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;

  points_collection: any;
  points: any;

  
  addCollectionForm = new FormGroup({

    value: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    description: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl('active'),

  
    
    
  })
  

  


  addPointsForm = new FormGroup({

    value: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    
    type: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    community_member_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    activity_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
  
    
  
  
    
    
  })
  selected_points: any;
  member_list: any;
  collection_status: { id: string; label: string; value: string; class: string; }[];
  tab_menu_list: { id: string; label: string; class: any; }[];
  leaderboard: any[];

  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")

      if( this.tab_view  == 'collection'){
        this.getCommunityPointsCollection()
      }
      
      if( this.tab_view  == 'points_activity'){
        this.getCommunityPoints()
      }
      if( this.tab_view  == 'leaderboard'){
        this.getPointsLeaderboard()
      }
    })
    this.leaderboard = []
    this.setComponentData()
    this.collection_status = [
  
      {
        'id' : 'status',
        'label' : 'active',
        'value' : 'active',
        'class' : 'paid',
      },
      {
        'id' : 'status',
        'label' : 'in_active',
        'value' : 'in_active',
        'class' : 'paid',
      },
    
    ]
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'collection',
        'label' : 'Collection',
      
        'class' : null,
      },
     
      {
        'id' : 'points_activity',
        'label' : 'Points Activity',
      
        'class' : null,
      },
      {
        'id' : 'leaderboard',
        'label' : 'Leaderboard',
     
        'class' : null,
      },


     
    ]
    
  }

  selectMember(e){
    console.log(e)
    this.addPointsForm.get('community_member_id').setValue(e.value)
  }

  
  async addCommunityPointsCollection(){
    console.log(this.addCollectionForm.value)

    try {
      this.appService.showLoading()
      let response = await this.webService.addCommunityPointsCollection(this.addCollectionForm.value)
      console.log(response)
      if(response.data){
        this.appService.openToast('Collection Added')
        this.getCommunityPointsCollection()
      }
    
  
    } catch (e) {
      this.appService.openToast(e)
    }
    finally{
      this.appService.hideLoading()
      this.addCollectionForm.reset()
      this.closeModal()
    }
  }


  async updateCommunityPointsCollection(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
      this.appService.showLoading()
      let res = await this.webService.updateCommunityPointsCollection(form,id)
      if(res.data){
        this.appService.openToast('Updated!')
      }else{
        this.getCommunityPointsCollection()
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.closeModal()
      this.appService.hideLoading()
    
    }
  }


  async addMemberPoints(){
    console.log(this.addPointsForm.value)

    try {
      this.appService.showLoading()
      let response = await this.webService.addMemberPoints(this.addPointsForm.value)
      console.log(response)
      if(response.data){
        this.appService.openToast('Points Added')
      }
    
  
    } catch (e) {
      this.appService.openToast(e)
    }
    finally{
      this.appService.hideLoading()
      this.addPointsForm.reset()
      this.closeModal()
    }
  }
  async searchMember(e){
    try {
      this.member_list = []
      this.appService.showLoading()
      let res = await this.webService.getCommunityMember()
      console.log(res)
      res.data.forEach(member => {
        this.member_list.push(
          {
            'id':'community_member_id',
            'label': member.first_name +' '+  member.last_name,
            'value':member.id
          }
        )  
      });
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }


  sendPoints(item){
    this.selected_points = item
    console.log(this.selected_points)
    this.addPointsForm.get('value').setValue(item.value)
    this.addPointsForm.get('type').setValue('credit')
    this.addPointsForm.get('activity_id').setValue(item.id)
    this.openModal('addPointModal')
  }

  async getCommunityPoints(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityPoints()
      this.points = res.data
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }


  
  async getPointsLeaderboard(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getPointsLeaderboard()
       this.leaderboard = res.data
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }


    
  async getCommunityPointsCollection(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityPointsCollection()
       this.points_collection = res.data
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }




  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }


}
