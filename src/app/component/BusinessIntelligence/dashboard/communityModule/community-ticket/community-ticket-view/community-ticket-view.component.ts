import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-community-ticket-view',
  templateUrl: './community-ticket-view.component.html',
  styleUrls: ['./community-ticket-view.component.scss']
})
export class CommunityTicketViewComponent implements OnInit {
  ticket_id: string;
  ticket: any;
  faChevronLeft=faChevronLeft
  ticket_status: { id: string; label: string; value: string; class: string; }[];
  priority_status: { id: string; label: string; value: string; class: string; }[];
  topic_status: { id: string; label: string; value: string; class: string; }[];
  message: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params=>{
      this.ticket_id = params.get("id")
      this.getCommunityTicketById(this.ticket_id)
     
    })

    

   this.ticket_status = [
    {
      'id' : 'status',
      'label' : 'new_ticket',
      'value' : 'new_ticket',
      'class' : 'paid',
    },
    {
      'id' : 'status',
      'label' : 'on_process',
      'value' : 'on_process',
      'class' : 'paid',
    },
    {
      'id' : 'status',
      'label' : 'complete',
      'value' : 'complete',
      'class' : 'paid',
    },
  
  ]

  
  this.priority_status = [
    {
      'id' : 'priority',
      'label' : 'low',
      'value' : 'low',
      'class' : 'paid',
    },
    {
      'id' : 'priority',
      'label' : 'medium',
      'value' : 'medium',
      'class' : 'paid',
    },
    {
      'id' : 'priority',
      'label' : 'high',
      'value' : 'high',
      'class' : 'paid',
    },
  
  ]

  this.topic_status = [
    {
      'id' : 'topic',
      'label' : 'points',
      'value' : 'points',
      'class' : 'paid',
    },
    {
      'id' : 'topic',
      'label' : 'reward',
      'value' : 'reward',
      'class' : 'paid',
    },
    {
      'id' : 'topic',
      'label' : 'account',
      'value' : 'account',
      'class' : 'paid',
    },
    {
      'id' : 'topic',
      'label' : 'campaign',
      'value' : 'campaign',
      'class' : 'paid',
    },
  ]
  }

  async updateTicket(item){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
     
      let res = await this.webService.updateTicket(this.ticket_id,form)
      if(res.data){
        this.appService.openToast('Updated!')
       
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
   
    }
   
  }


  
  async getCommunityTicketById(id){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityTicketById(id)
      this.ticket = res.data
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }


  
  async addTicketDiscussion(){
    
    let form = {
      'account_id' : 1,
      'community_ticket_id' : this.ticket_id,
      'message' : this.message
    }

    try {
      
      let response = await this.webService.addTicketDiscussion(form)
   
      console.log(response)
      this.appService.openToast('Send!')
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.message = null
      this.getCommunityTicketById(this.ticket_id)
  
    }
  }


}
