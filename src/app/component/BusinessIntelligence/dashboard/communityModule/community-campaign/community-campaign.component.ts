import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-community-campaign',
  templateUrl: './community-campaign.component.html',
  styleUrls: ['./community-campaign.component.scss']
})
export class CommunityCampaignComponent implements OnInit {
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  campaign: any;

  
  campaignForm = new FormGroup({
    campaign_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    description: new FormControl(),
    start_date: new FormControl(null),
    end_date: new FormControl(null),

    client: new FormControl(null),
    quota: new FormControl(null),
    
    status: new FormControl('draft'),

    reward_value: new FormControl(null),


    community_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),



  
  
    
    
  })
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")
    })
    
   this.getCommunityCampaign()
  }


  async getCommunityCampaign(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityCampaign()
      this.campaign = res.data
      this.campaign.forEach(item => {
        item.total_participant = this._getParticipant(item.participant)
      });
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  


  _getParticipant(items){
    let total = 0
    items.forEach(item => {
      if(item.status == 'approve'){
        total += 1
      }
     
    });

    return total
  }
  async addCommunityCampaign(){
    console.log(this.campaignForm.value)
    try {
      this.appService.showLoading()
      let res = await this.webService.addCommunityCampaign(this.campaignForm.value)
      this.campaign.unshift(res.data)
      console.log(res)

      this._navigateTo(res.data.id)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.campaignForm.reset()
      this.closeModal()
      this.appService.hideLoading()
    
    }
  }



  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }


  _navigateTo(page) {
    this.router.navigate(
      ['/community/campaign/view/'+page]
    );
  }


}
