import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityCampaignViewComponent } from './community-campaign-view.component';

describe('CommunityCampaignViewComponent', () => {
  let component: CommunityCampaignViewComponent;
  let fixture: ComponentFixture<CommunityCampaignViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityCampaignViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityCampaignViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
