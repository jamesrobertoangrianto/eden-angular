import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-community-rewards',
  templateUrl: './community-rewards.component.html',
  styleUrls: ['./community-rewards.component.scss']
})
export class CommunityRewardsComponent implements OnInit {

  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];
  booking_status: { id: string; label: string; class: string; }[];
  tab_view: string;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  reward: any;
  redeem: any;

  
  rewardForm = new FormGroup({
    name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    description: new FormControl(null),

    value: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    redeem_points: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    quota: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    start_date: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    end_date: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),



  
    
    
  })

  redeemForm = new FormGroup({
    status: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    community_member_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    community_reward_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
  
  
  
    
    
  })
  reward_status: { id: string; label: string; value: string; class: string; }[];
  selected_reward: any;
  member_list: any[];
  selected_member: any;

  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

   
  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")

      if( this.tab_view  == 'collection'){
        this.getReward()
      }
      
      if( this.tab_view  == 'redeem'){
        this.getRewardRedeem()
      }
    })
    
    this.setComponentData()

    
   this.reward_status = [
  
    {
      'id' : 'status',
      'label' : 'redeem',
      'value' : 'redeem',
      'class' : 'paid',
    },
    {
      'id' : 'status',
      'label' : 'reward_sent',
      'value' : 'reward_sent',
      'class' : 'paid',
    },
  
  ]
  }

  memberRedeem(item){
    console.log( item)
    this.selected_reward = item
    this.redeemForm.get('community_reward_id').setValue(item.id)
    this.redeemForm.get('status').setValue('redeem')
   
    this.openModal('memberRedeemModal')
  }



  
  async addMemberRedeem(){
    console.log(this.redeemForm.value)
    try {
      this.appService.showLoading()
      let response = await this.webService.addMemberRedeem(this.selected_reward.id,this.selected_member.value,this.redeemForm.value)
      console.log(response)
    
    
  
    } catch (e) {
      this.appService.openToast(e)
      console.log(e)
    
    }
    finally{
      this.appService.hideLoading()
      this.redeemForm.reset()
      this.closeModal()
    }
  }


  selectMember(e){
    console.log(e)
    this.selected_member = e
    this.redeemForm.get('community_member_id').setValue(e.value)
  }


  async searchMember(e){
    try {
      this.member_list = []
      this.appService.showLoading()
      let res = await this.webService.getCommunityMember()
      console.log(res)
      res.data.forEach(member => {
        this.member_list.push(
          {
            'id':'community_member_id',
            'label': member.first_name +' '+  member.last_name,
            'value':member.id
          }
        )  
      });
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }


  setComponentData(){
    this.tab_menu_list = [
      {
        'id' : 'collection',
        'label' : 'Collection',
        'count' : 10,
        'class' : null,
      },
      {
        'id' : 'redeem',
        'label' : 'Redeem',
        'count' : 10,
        'class' : null,
      },
    
     
    ]
    
  }
  async addReward(){
    try {
      this.appService.showLoading()
      let res = await this.webService.addReward(this.rewardForm.value)
      if(res.data){
        this.reward.unshift(res.data)
        this.appService.openToast('New reward added!')
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.closeModal()
      this.appService.hideLoading()
    
    }
  }


  async updateRewardRedeem(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
      this.appService.showLoading()
      let res = await this.webService.updateRewardRedeem(form,id)
      if(res.data){
        this.appService.openToast('Updated!')
      }else{
        this.getRewardRedeem()
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.closeModal()
      this.appService.hideLoading()
    
    }
  }



  
  async getReward(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getReward()
      this.reward = res.data
      console.log(res)
   console.log('res')
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

    
  async getRewardRedeem(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getRewardRedeem()
      this.redeem = res.data
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }





  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }



}
