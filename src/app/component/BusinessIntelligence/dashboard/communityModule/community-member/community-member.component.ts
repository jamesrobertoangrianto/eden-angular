import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload, faPhone, faEnvelope, faNotEqual, faStickyNote, faPenSquare, faExpandAlt } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-community-member',
  templateUrl: './community-member.component.html',
  styleUrls: ['./community-member.component.scss']
})
export class CommunityMemberComponent implements OnInit {

  faExpandAlt=faExpandAlt
  faChevronDown = faChevronDown
  faPenSquare=faPenSquare
  faStickyNote=faStickyNote
  faEnvelope= faEnvelope
  faPhone = faPhone
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract_status: { id: string; label: string; class: string; }[];

  tab_view: string;
  tab_menu_list: { id: string; label: string; count: number; class: any; }[];
  member: any;
  member_status: { id: string; label: string; value: string; class: string; }[];


  memberForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    last_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    community_id: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_account: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    instagram_follower: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    status: new FormControl('new_request'),

  
  
    
    
  })
  community: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }


  
  
  ngOnInit(): void {
    
   this.getCommunityMember()
    this.getCommunity()

   this.member_status = [
    {
      'id' : 'status',
      'label' : 'new_request',
      'value' : 'new_request',
      'class' : 'paid',
    },
    {
      'id' : 'status',
      'label' : 'active',
      'value' : 'active',
      'class' : 'paid',
    },
    {
      'id' : 'status',
      'label' : 'in_active',
      'value' : 'in_active',
      'class' : 'paid',
    },
  
  ]
  }

  async getCommunity(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunity()
      this.community = res.data
    
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  async getCommunityMember(){
    try {
      this.appService.showLoading()
      let res = await this.webService.getCommunityMember()
      this.member = res.data
      this.member.forEach(item => {
        item.total_campaign = this._getCampaign(item.campaign)
      });
      console.log(res)
   
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.appService.hideLoading()
    
    }
  }

  
  async updateCommunityMember(item,id){
    let form ={}
    form[item.id] = item.value
    console.log(item)
    try {
     
      let res = await this.webService.updateCommunityMember(form,id)
      if(res.data){
          this.appService.openToast('Updated!')
      }
      console.log(res)
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
   
    }
    
   
  }



  _getCampaign(items){
    let total = 0
    items.forEach(item => {
      if(item.status == 'approve'){
        total += 1
      }
     
    });

    return total
  }


  async addCommunityMember(con){
    try {
      this.appService.showLoading()
      let res = await this.webService.addCommunityMember(this.memberForm.value)
     console.log(res)
      if(res.data.id){
      
        if(con){
          this._navigateTo(res.data.id)
        }
        else{
          this.getCommunityMember()
          this.getCommunity()
        }
        this.appService.openToast('New Member Added!')
     
      }
    
    
    } catch (error) {
      this.appService.openToast(error)
    }
    finally{
      this.memberForm.reset()
      this.appService.hideLoading()
      this.closeModal()
    }
  }


  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }


  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/community/member/view/'+page]
    );
  }


}
