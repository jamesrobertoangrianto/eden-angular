import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faPhone, faEnvelope, faMapPin, faChevronDown, faDotCircle, faChevronCircleRight, faChevronRight, faColumns, faExpandAlt, faExpandArrowsAlt, faExpand } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {
  creator_rate = {
    'platform_name' : null,
    'rate_name' : null,
    'rate_price' : null,
    'creator_id' : null,
  }

  tier = [
    {
      'id' : 'Nano',
      'value' : '( 1000 - 9999 )'
    },
    {
      'id' : 'Micro',
      'value' : '( 10000 - 99999 )'
    },
    {
      'id' : 'Macro',
      'value' : '( 100000 - 999999 )'
    },
    {
      'id' : 'Mega',
      'value' : '( >= 1000000 )'
    }

  ]
  table_columns = [
    {
      'name' : 'Overview',
      'id' : 'overview',
      'is_default' : 1,
      'icon' : 'icon'
    },
  
    {
      'name' : 'Personal Info',
      'id' : 'personal',
      'is_default' : 1,
      'icon' : 'icon'
    },

    {
      'name' : 'Performance',
      'id' : 'performance',
      'is_default' : 1,
      'icon' : 'icon'
    },
    {
      'name' : 'Instagram Metric',
      'id' : 'instagram_metric',
      'is_default' : 1,
      'icon' : 'icon'
    },

    {
      'name' : 'Instagram Rate',
      'id' : 'instagram_rate',
      'is_default' : 1,
      'icon' : 'icon'
    },
    {
      'name' : 'Tiktok Metric',
      'id' : 'tiktok_metric',
      'is_default' : 1,
      'icon' : 'icon'
    },
    
    {
      'name' : 'Tiktok Rate',
      'id' : 'tiktok_rate',
      'is_default' : 1,
      'icon' : 'icon'
    }
  ]

  createContactForm = new FormGroup({
    first_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    last_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    alias: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    dob: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    location: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),



    tier: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),


    company_type: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    company_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

    create_by: new FormControl(null),

    


    
  })
  faExpandAlt= faExpandAlt
  faPhone=faPhone
  faEnvelope= faEnvelope
  faMapPin =faMapPin
  faChevronDown = faChevronDown
  faDotCircle=faDotCircle
  faChevronRight=faChevronRight
  faColumns=faColumns
  show_columns: boolean;
  columns_view: string;
  selected_columns: any;
  showModal: any;
  showMiniModal: any;
  alias: boolean;
  creator: any;
  selected_creator: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {
      this.columns_view = queryParams.get("columns_view")
      if(!this.columns_view){
      
        this.columns_view = 'overview'
      }
    })

    this.getCreator()
  }

  

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }

  viewRate(item){
    this.selected_creator = item
    this.openModal('rateModal')
  }

  viewProject(item){
    this.selected_creator = item
    this.openModal('projectModal')
  }




  viewSosmed(item){
    this.selected_creator = item
  }

  viewContact(item){
    this.selected_creator = item
    this.openModal('contactModal')
  }



  
  async getCreator(){
    
    
    try {
      this.appService.showLoading()
      let response = await this.webService.getCreator()
    
      this.creator = response.data
      this.creator.forEach(creator => {
        creator.performance = this._getPerformance(creator.performance_score)
      });
      console.log(this.creator)
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.appService.hideLoading()
  
    }
  }



  _getPerformance(total){
    

  
      if( total > 67 && total < 100){
        return 'best'
      }
      else if( total > 37 && total < 67){
        return 'good'
      }
      else if( total >0 && total< 37){
        return 'bad' 
      }
      else{
        return 'not set'
      }
      
     
      
     
  }

  async addCreator(isView){
    
    console.log(this.createContactForm.value)

    this.createContactForm.get('create_by').setValue(this.webService.account_id)
    try {
    this.appService.showLoading()
      let response = await this.webService.addCreator(this.createContactForm.value)
      
      if(isView){
        this._navigateTo(response.data.id)
      }else{
        this.getCreator()
      }

    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.createContactForm.reset
      this.appService.hideLoading()
      this.closeModal()
      this.appService.openToast('New creator added!')
    
      
    }
  }

  
  


  async addNewRateCart(){


  
    try {
        this.creator_rate.creator_id = this.selected_creator.id
      let response = await this.webService.addCreatorRate(this.creator_rate,this.selected_creator.id)
      this.appService.openToast('New Rate Added')
    
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
      this.selected_creator.rate.push(this.creator_rate)
      this.closeMiniModal()
    }
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }


  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/creator/view/'+page]
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
  
  }

  openMiniModal(modal){
    this.showMiniModal = modal
  }
  closeMiniModal(){
    this.showMiniModal = null
  }
  toggleAlias(){
    this.alias = !this.alias
  }

}
