import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faChevronDown, faUserPlus, faChevronLeft, faCalendarAlt, faMapMarker, faChevronRight, faUpload } from '@fortawesome/free-solid-svg-icons';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {
  faChevronDown = faChevronDown
  faUpload= faUpload
  faChevronRight=faChevronRight
  faUserPlus = faUserPlus
  faChevronLeft=faChevronLeft
  faCalendar=faCalendarAlt
  faMapMarked =faMapMarker
  show_columns: boolean;
  selected_columns: any;
  columns_view: string;
  showModal: any;
  payment_status: {}[];
  contract: any;
  contract_status: { id: string; label: string; value: string; }[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService : BusinessIntelligenceService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    

    this.contract_status = [
      {
        'id' : 'status',
        'label' : 'draft',
        'value' : 'draft',
      
      },
      {
        'id' : 'status',
        'label' : 'not-signed',
        'value' : 'not-signed',
      },
      {
        'id' : 'status',
        'label' : 'signed',
        'value' : 'signed',
      },
      {
        'id' : 'status',
        'label' : 'terminated',
        'value' : 'terminated',
      }
    ]

    this.getContract()

  }




  async getContract(){
    
    
    try {
    
      let response = await this.webService.getContract()
      this.contract = response.data

     
    console.log(this.contract)
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
  
    }
  }

  getPlaceholderName(item) {
    if(item.first_name && item.last_name){
      var first_name =item.first_name.charAt(0);
      var last_name =item.last_name.charAt(0);
      return first_name + last_name
    }
  
  }

  toggleShow(item){
    item.show = !item.show
  }
  async updateContract(item,id){
    console.log(id)
    let form = {}
    form[item.id] = item.value
    try {
    
    let response = await this.webService.updateContract(form,id)
      if(response.data){
        this.appService.openToast('Updated!')
      }
     
    } catch (e) {
      
      this.appService.openToast(e)
      
      console.log(e)
    } finally {
    
  
    }
  }
  toJson(item){
    return JSON.parse(item)
  }

  openModal(modal){
    this.showModal = modal
  }
  closeModal(){
    this.showModal = null
  }

  toggleColumnsMenu(){
    this.show_columns = !this.show_columns
  }

  _navigateTo(page) {
    this.router.navigate(
      ['/creator/contract/view/'+page]
    );
  }

  changeColumnsView(item) {
    this.router.navigate(
      [],
      { queryParams: { columns_view: item.id},
      queryParamsHandling: 'merge' }
    );
    this.selected_columns = item
    this.toggleColumnsMenu()
   
  }
  
}
