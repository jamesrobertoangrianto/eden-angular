export interface Notification{
    notification_id: string,
    fcm_id: string,
    customer_id: string,
    cart_id: string,
    behavior: string,
}