import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DeviceDetectorModule } from 'ngx-device-detector';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BIDashboardComponent } from './component/BusinessIntelligence/dashboard/dashboard.component';


import { BISidebarComponent } from './component/BusinessIntelligence/dashboard/shareModule/bisidebar/bisidebar.component';
import { LoadingComponentComponent } from './component/BusinessIntelligence/dashboard/shareModule/loading-component/loading-component.component';
import { ModalComponent } from './component/BusinessIntelligence/dashboard/shareModule/modal/modal.component';
import { ProgressComponentComponent } from './component/BusinessIntelligence/dashboard/shareModule/progress-component/progress-component.component';
import { ToastComponentComponent } from './component/BusinessIntelligence/dashboard/shareModule/toast-component/toast-component.component';
import { TopNavigationComponent } from './component/BusinessIntelligence/dashboard/shareModule/top-navigation/top-navigation.component';
import { LoginComponent } from './component/BusinessIntelligence/dashboard/userModule/login/login.component';
import { ProfileComponent } from './component/BusinessIntelligence/dashboard/userModule/profile/profile.component';
import { UserAccountComponent } from './component/BusinessIntelligence/dashboard/userModule/user-account/user-account.component';
import { UserRoleComponent } from './component/BusinessIntelligence/dashboard/userModule/user-role/user-role.component';
import { BodyComponent } from './component/BusinessIntelligence/dashboard/shareModule/body/body.component';
import { BodyFullComponent } from './component/BusinessIntelligence/dashboard/shareModule/body-full/body-full.component';
import { PlaceholderComponent } from './component/BusinessIntelligence/dashboard/shareModule/placeholder/placeholder.component';
import { PaginationComponent } from './component/BusinessIntelligence/dashboard/shareModule/pagination/pagination.component';
import { TableActionComponent } from './component/BusinessIntelligence/dashboard/shareModule/table-action/table-action.component';
import { CampaignComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign.component';
import { ContractComponent } from './component/BusinessIntelligence/dashboard/creatorModule/contract/contract.component';
import { CampaignViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/campaign/campaign-view/campaign-view.component';
import { ModalFullComponent } from './component/BusinessIntelligence/dashboard/shareModule/modal-full/modal-full.component';
import { ModalSheetsComponent } from './component/BusinessIntelligence/dashboard/shareModule/modal-sheets/modal-sheets.component';
import { StatusLabelComponent } from './component/BusinessIntelligence/dashboard/shareModule/status-label/status-label.component';
import { SalesPipelineComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline.component';
import { SalesPipelineViewComponent } from './component/BusinessIntelligence/dashboard/clientModule/sales-pipeline/sales-pipeline-view/sales-pipeline-view.component';
import { ClientComponent } from './component/BusinessIntelligence/dashboard/clientModule/client/client.component';
import { NewsfeedBackendComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend.component';
import { ChannelVendorComponent } from './component/BusinessIntelligence/dashboard/channelModule/channel-vendor/channel-vendor.component';
import { InventoryComponent } from './component/BusinessIntelligence/dashboard/channelModule/inventory/inventory.component';
import { EmployeeComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee.component';
import { CommunityCampaignComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign.component';
import { CommunityRewardsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-rewards/community-rewards.component';
import { CommunityTicketComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket.component';
import { CommunityMemberComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member.component';
import { TabMenuComponent } from './component/BusinessIntelligence/dashboard/shareModule/tab-menu/tab-menu.component';
import { AssetsComponent } from './component/BusinessIntelligence/dashboard/channelModule/assets/assets.component';
import { CommunityTicketViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-ticket/community-ticket-view/community-ticket-view.component';
import { CreativeAssignmentComponent } from './component/BusinessIntelligence/dashboard/creativeModule/creative-assignment/creative-assignment.component';
import { CreativeVendorComponent } from './component/BusinessIntelligence/dashboard/creativeModule/creative-vendor/creative-vendor.component';
import { CommunityPointsComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-points/community-points.component';
import { CommunityAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/community-account/community-account.component';
import { CommunityAccountRewardsComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/community-account/community-account-rewards/community-account-rewards.component';
import { CreatorAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creator-account/creator-account.component';
import { ChannelAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/channel-account/channel-account.component';
import { CreativeAccountComponent } from './component/BusinessIntelligence/dashboard/clientAreaModule/creative-account/creative-account.component';
import { NewsfeedBackendFilteringComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-filtering/newsfeed-backend-filtering.component';
import { NewsfeedBackendCategoryComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-category/newsfeed-backend-category.component';
import { NewsfeedBackendAccountComponent } from './component/BusinessIntelligence/dashboard/clientModule/newsfeed-backend/newsfeed-backend-account/newsfeed-backend-account.component';
import { GeneralContactComponent } from './component/BusinessIntelligence/dashboard/userModule/general-contact/general-contact.component';
import { TaskComponent } from './component/BusinessIntelligence/dashboard/shareModule/task/task.component';
import { FormInputComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-input/form-input.component';
import { FormSelectComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-select/form-select.component';
import { RemoveButtonComponent } from './component/BusinessIntelligence/dashboard/shareModule/remove-button/remove-button.component';
import { CreatorComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator.component';
import { CreatorViewComponent } from './component/BusinessIntelligence/dashboard/creatorModule/creator/creator-view/creator-view.component';
import { FormSearchComponent } from './component/BusinessIntelligence/dashboard/shareModule/form-search/form-search.component';
import { StatusLabelPerformanceComponent } from './component/BusinessIntelligence/dashboard/shareModule/status-label-performance/status-label-performance.component';
import { EmployeeViewComponent } from './component/BusinessIntelligence/dashboard/channelModule/employee/employee-view/employee-view.component';
import { CommunityMemberViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-member/community-member-view/community-member-view.component';
import { CommunityCampaignViewComponent } from './component/BusinessIntelligence/dashboard/communityModule/community-campaign/community-campaign-view/community-campaign-view.component';

@NgModule({
  declarations: [
    AppComponent,
    BIDashboardComponent,
    BISidebarComponent,
    TopNavigationComponent,
    ModalComponent,
    ProgressComponentComponent,
    ToastComponentComponent,
    LoadingComponentComponent,
    ProfileComponent,
    LoginComponent,
    UserAccountComponent,
    UserRoleComponent,
    BodyComponent,
    BodyFullComponent,
    PlaceholderComponent,
    PaginationComponent,
    TableActionComponent,
    CampaignComponent,
    ContractComponent,
    CampaignViewComponent,
    ModalFullComponent,
    ModalSheetsComponent,
    StatusLabelComponent,
    SalesPipelineComponent,
    SalesPipelineViewComponent,
    ClientComponent,
    NewsfeedBackendComponent,
    ChannelVendorComponent,
    InventoryComponent,
    EmployeeComponent,
    CommunityCampaignComponent,
    CommunityRewardsComponent,
    CommunityTicketComponent,
    CommunityMemberComponent,
    TabMenuComponent,
    AssetsComponent,
    CommunityTicketViewComponent,
    CreativeAssignmentComponent,
    CreativeVendorComponent,
    CommunityPointsComponent,
    CommunityAccountComponent,
    CommunityAccountRewardsComponent,
    CreatorAccountComponent,
    ChannelAccountComponent,
    CreativeAccountComponent,
    NewsfeedBackendFilteringComponent,
    NewsfeedBackendCategoryComponent,
    NewsfeedBackendAccountComponent,
    NewsfeedBackendComponent,
    GeneralContactComponent,
    TaskComponent,
    FormInputComponent,
    FormSelectComponent,
    RemoveButtonComponent,
    CreatorComponent,
    CreatorViewComponent,
    FormSearchComponent,
    StatusLabelPerformanceComponent,
    EmployeeViewComponent,
    CommunityMemberViewComponent,
    CommunityCampaignViewComponent
    
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DeviceDetectorModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,

    
  ],
  providers: [
   
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
